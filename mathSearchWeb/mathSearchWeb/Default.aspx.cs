﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using mathsearch;

namespace mathSearchWeb
{
    public partial class _Default : System.Web.UI.Page
    {
        private static XslCompiledTransform converter = new XslCompiledTransform();
        private Log log;
        protected void Page_Load(object sender, EventArgs e)
        {
            Rippling.LoadRules(Server.MapPath("rules"));
            ClientScript.RegisterStartupScript(this.GetType(), "MathJaxLoading", "<script type=\"text/javascript\" src=\"http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML\"> </script>");
            converter.Load(Server.MapPath("Scripts/mmlctop2_0.xsl"));
            log = new Log(Server.MapPath("logs/"+DateTime.Now.ToShortDateString()+".txt"));
        }

        struct ResultEntry
        {
            public Uri url;
            public int percentMatch;
            public string content;
            public ResultEntry(string u, int percent, string c)
            {
                url = new Uri(u);
                percentMatch = percent;
                content = c;
            }
            public override string ToString()
            {

                return url + " " + percentMatch;
            }
        }

        private static int CompareEntries(ResultEntry e1, ResultEntry e2)
        {
            if (e1.percentMatch > e2.percentMatch)
                return 1;
            else
                if (e1.percentMatch < e2.percentMatch)
                    return -1;
                else
                    return 0;
        }

        private static Comparison<ResultEntry> ComparisonOfEntries = new Comparison<ResultEntry>(CompareEntries);

        protected void searchBtn_Click(object sender, EventArgs e)
        {
            SqlDataSource sql = new SqlDataSource("Data Source=.\\SQLEXPRESS;Initial Catalog=Expressions;Integrated Security = True;", "");
            List<ResultEntry> searchResults = new List<ResultEntry>();
            try
            {

                string input = mathInput.Text;
                Expression searchInput = new Expression(input);
                //
                XmlReader reader = XmlReader.Create(new System.IO.StringReader(mathInput.Text));
                System.IO.StringWriter writer = new System.IO.StringWriter();
                converter.Transform(reader, XmlWriter.Create(writer));
                string convertedMathML = writer.ToString();
                if (convertedMathML.Length < 130)
                    inputRender.Text = "<font size = 32> " + convertedMathML + "</font>";
                else
                    inputRender.Text = "<font size = 8> " + convertedMathML + "</font>";

                //search
                sql.SelectCommand = "SELECT * FROM List WHERE varCount=" + searchInput.varCount;
                var result = sql.Select(new DataSourceSelectArguments());
                foreach (System.Data.DataRowView p in result)
                {
                    try
                    {
                        Expression cmp = new Expression((string)p.Row.ItemArray[3]);
                        double match = Rippling.Eq(searchInput, cmp,0.8,true, true, MeasureComparisonMode.Shrunk);
                        if (match > 0)
                        {
                            System.IO.StringWriter resultWriter = new System.IO.StringWriter();
                            converter.Transform(XmlReader.Create(new System.IO.StringReader((string)p.Row[3])), XmlWriter.Create(resultWriter));
                            searchResults.Add(new ResultEntry((string)p.Row[2], (int)(match * 100), resultWriter.ToString()));
                        }
                    }
                    catch (Exception ex)
                    {
                        //logging
                        log.Add("Error: "+ex.Message);
                    }

                }
                searchResults.Sort(ComparisonOfEntries);
                if (searchResults.Count == 0)
                    searchResultsPanel.Controls.Add(new LiteralControl("Nothing relevant was found"));
                else
                    searchResultsPanel.Controls.Add(new LiteralControl(searchResults.Count + " results</br>"));
                for (int i = searchResults.Count - 1; i >= 0; i--)
                {
                    ResultEntry entry = searchResults[i];
                    //Results.Items.Add(entry.ToString());
                    searchResultsPanel.Controls.Add(new LiteralControl("№" + (searchResults.Count - i) + " <a href=\"" + entry.url.AbsoluteUri + "\" target=\"_blank\">" + entry.url.ToString() + "</a> with " + entry.percentMatch.ToString() + "% match. </br>Formula: "));
                    searchResultsPanel.Controls.Add(new LiteralControl(entry.content.ToString()));
                    searchResultsPanel.Controls.Add(new LiteralControl("<br /><br />"));
                }
            }
            catch (Exception ex)
            {
                //logging
                inputRender.Text = "Error occured. " + ex.Message;
            }

        }
    }
}
