﻿<%@ Page Title="Math search engine" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="mathSearchWeb._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:TextBox ID="mathInput" runat="server" Width="430px" Height="132px" 
        TextMode="MultiLine" />
    Input:
    <asp:Literal ID="inputRender" runat="server"></asp:Literal>
    <br />
    <asp:HyperLink ID="HyperLink1" runat="server" 
        NavigateUrl="http://cnx.org/math-editor/popup" Target="_blank">Convenient Content MathML editor</asp:HyperLink>
    <br />
    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    <br />
    <asp:Button ID="searchBtn" runat="server" Text="Search" Width="91px" onclick="searchBtn_Click" />
    <asp:Panel ID="searchResultsPanel" runat="server">
    </asp:Panel>
    <br />
    </asp:Content>
