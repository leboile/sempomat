﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using mathsearch;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace UnitTest
{
    [TestClass]
    public class CreateWaveRules
    {
        [TestMethod]
        public void MeasureTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            Measure m = null;
            DateTime before = DateTime.Now;
            m = new Measure(t1);
            DateTime after = DateTime.Now;
            Console.WriteLine("Straight: " + m + " ms: " + (after - before).Milliseconds);
            Console.WriteLine("Imprint: " + new Measure(new Imprint(t1)) + " inverted: "+ new Measure(new Imprint(t1).InvertedCopy()));
            Console.WriteLine("Inverted: " + new Measure(t1,true));
            Console.WriteLine("Shrunk: "+m.Shrunk(Skeleton.GetDepthOfNodeInSkeleton(t1.Right)));
            return;
        }

        [TestMethod]
        public void ShrunkMeasureTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("p", MarkupType.Skeleton));
            t1.Add(new Function("h", MarkupType.WaveFront));
            t1.Left.Add(new Function("g", MarkupType.WaveFront));
            t1.Left.Left.Add(new Function("f", MarkupType.Skeleton));
            t1.Left.Left.Left.Add(new Variable("c", MarkupType.Skeleton));

            BinaryTree t2 = new BinaryTree(new Function("p", MarkupType.Skeleton));
            t2.Add(new Function("h", MarkupType.WaveFront));
            t2.Left.Add(new Function("g", MarkupType.Skeleton));
            t2.Left.Left.Add(new Function("f", MarkupType.Skeleton));
            t2.Left.Left.Left.Add(new Variable("c", MarkupType.Skeleton));
            Measure m1 = new Measure(t1);
            Measure m2 = new Measure(t2);
            Console.WriteLine("t1: " + m1 + " t2: "+m2);
            Console.WriteLine("Shrunk: " + m2.Shrunk(Skeleton.GetDepthOfNodeInSkeleton(t2.Left.Left)));
            return;
        }

        /// <summary>
        /// Given: 
//plus Skeleton
// plus WaveFront
//  a00 WaveFront
//  a10 Skeleton
// b11 Skeleton

//Goal: 
//plus Skeleton
// a10 Skeleton
// plus WaveFront
//  b11 WaveFront
//  b11 WaveFront
        /// </summary>
        [TestMethod]
        public void SkeletonEqualsTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a00", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a10", MarkupType.Skeleton));
            t1.Add(new Variable("b11", MarkupType.Skeleton));

            BinaryTree t2 = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            t2.Add(new Variable("a10", MarkupType.Skeleton));
            t2.Add(new Function("plus", MarkupType.WaveFront));
            t2.Right.Add(new Variable("b10", MarkupType.WaveFront));
            t2.Right.Add(new Variable("b10", MarkupType.WaveFront));
            Console.WriteLine(new Skeleton(t1));
            Console.WriteLine("-0----------");
            Console.WriteLine(new Skeleton(t2));
            Assert.IsFalse(Rippling.isSkeletonPreserving(t1, t2));
        }

        [TestMethod]
        public void WaveRuleCreation()
        {
            return;
            //WaveRule.CreateWaveRules("WR/1sumcommutativity1.txt", "WR/1sumcommutativity2.txt", "sumcommut1");
            //WaveRule.CreateWaveRules("WR/1sumcommutativity2.txt", "WR/1sumcommutativity1.txt", "sumcommut2");
            //WaveRule.CreateWaveRules("WR/2sumassoc1.txt", "WR/2sumassoc2.txt", "sumassoc1");
            //WaveRule.CreateWaveRules("WR/2sumassoc2.txt", "WR/2sumassoc1.txt", "sumassoc2");
            //WaveRule.CreateWaveRules("WR/3zero1.txt", "WR/3zero2.txt", "zero1");
            //WaveRule.CreateWaveRules("WR/3zero2.txt", "WR/3zero1.txt", "zero2");
            //// WaveRule.CreateWaveRules("WR/4suminverse1.txt", "WR/4suminverse2.txt", "suminverse1");
            ////WaveRule.CreateWaveRules("WR/4suminverse2.txt", "WR/4suminverse1.txt", "suminverse2");
            //WaveRule.CreateWaveRules("WR/5mulcommutativity1.txt", "WR/5mulcommutativity2.txt", "mulcommut1");
            //WaveRule.CreateWaveRules("WR/5mulcommutativity2.txt", "WR/5mulcommutativity1.txt", "mulcommut2");
            //WaveRule.CreateWaveRules("WR/6mulassoc1.txt", "WR/6mulassoc2.txt", "mulassoc1");
            //WaveRule.CreateWaveRules("WR/6mulassoc2.txt", "WR/6mulassoc1.txt", "mulassoc2");
            //WaveRule.CreateWaveRules("WR/7one1.txt", "WR/7one2.txt", "one1");
            //WaveRule.CreateWaveRules("WR/7one2.txt", "WR/7one1.txt", "one2");
            ////WaveRule.CreateWaveRules("WR/8mulinverse1.txt", "WR/8mulinverse2.txt", "mulinverse1");
            ////WaveRule.CreateWaveRules("WR/8mulinverse2.txt", "WR/8mulinverse1.txt", "mulinverse2");
            //WaveRule.CreateWaveRules("WR/9distrib1.txt", "WR/9distrib2.txt", "distrib1");
            //WaveRule.CreateWaveRules("WR/9distrib2.txt", "WR/9distrib1.txt", "distrib2");
        }
        [TestMethod]
        public void DeepCopyWholeTreeTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            BinaryTree foo;
            Console.WriteLine(t1.Left.Left.Data + " " + t1.Left.Left.DeepCopyWholeTree(out foo).Data);
            Console.WriteLine(t1.Right.Left.Data + " " + t1.Right.Left.DeepCopyWholeTree(out foo).Data);
            Console.WriteLine(t1.Left.Right.Data + " " + t1.Left.Right.DeepCopyWholeTree(out foo).Data);
            Assert.AreEqual(t1.Left.Left.Data, t1.Left.Left.DeepCopyWholeTree(out foo).Data);
            Assert.AreEqual(t1.Right.Left.Data, t1.Right.Left.DeepCopyWholeTree(out foo).Data);
            Assert.AreEqual(t1.Left.Right.Data, t1.Left.Right.DeepCopyWholeTree(out foo).Data);
            Assert.AreNotSame(t1.Left.Right, t1.Left.Right.DeepCopyWholeTree(out foo));
        }

        [TestMethod]
        public void BinaryTreePropertiesTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Left.Right.Root, t1));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Root, t1));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Left.Right, t1.Left.Left.Sibling));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Left.Left, t1.Left.Right.Sibling));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Left, t1.Right.Sibling));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Left.Left, t1.NodeByPath(t1.Left.Left.PathFromRoot)));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Right.Left, t1.NodeByPath(t1.Right.Left.PathFromRoot)));
            Assert.IsTrue(Assert.ReferenceEquals(t1, t1.NodeByPath(t1.PathFromRoot)));
            Assert.IsNull(t1.Sibling);
            Console.WriteLine(t1);
        }

    }
}
