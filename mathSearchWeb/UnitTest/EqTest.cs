﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using mathsearch;
namespace UnitTest
{
    [TestClass]
    public class EqTest
    {
        string txt7 = "<math><apply><plus/><apply><plus/><ci>a</ci><ci>b</ci></apply><ci>c</ci></apply></math>";
        string txt8 = "<math><apply><plus/><ci>a</ci><apply><plus/><ci>b</ci><ci>c</ci></apply></apply></math>";
        string txt9 = "<math><apply><times/><apply><plus/><ci>a</ci><ci>b</ci></apply><apply><plus/><ci>c</ci><ci>d</ci></apply></apply></math>";
        string txt10 = "<math><apply><plus/><apply><plus/><apply><times/><ci>a</ci><ci>c</ci></apply><apply><times/><ci>b</ci><ci>c</ci></apply></apply><apply><plus/><apply><times/><ci>a</ci><ci>d</ci></apply><apply><times/><ci>b</ci><ci>d</ci></apply></apply></apply></math>";
        string txt12 = "<math><apply><plus/><apply><times/><apply><plus/><ci>a</ci><ci>b</ci></apply><ci>c</ci></apply><apply><times/><apply><plus/><ci>a</ci><ci>b</ci></apply><ci>d</ci></apply></apply></math>";

        [TestMethod]
        public void AnnotationsStronglyMinimalTest()
        {
            try
            {
                List<UnificationEntry> unifs = Markup.Annotate(new Expression(txt7, true), new Expression(txt8, true), false, MinimalityOption.CostAndSubstMinimal);
                //List<UnificationEntry> unifs = Markup.Annotate(new Expression(txt7,true), new Expression(txt8,true), false);
                Console.WriteLine(unifs.Count);
                int minimalCost = int.MaxValue;
                int count = 0;
                foreach (UnificationEntry UE in unifs)
                {
                    //if (UE.SubstCount == 0)
                    //{
                        count++;
                        if (UE.WRCost < minimalCost)
                            minimalCost = UE.WRCost;
                        Console.WriteLine(UE.SubstCount + " "+UE.WRCost);
                    //}
                    Assert.IsTrue(UE.CompleteUnification() && Rippling.isSkeletonPreserving(UE.GivenRoot, UE.GoalRoot));
                }
                Console.WriteLine("Minimal cost: " + minimalCost);
                int j = 0;
                foreach (UnificationEntry UE in unifs)
                    if (UE.WRCost == minimalCost)
                        Console.WriteLine("#" + (++j) + " " + UE + "\r\n");
            }
            catch (NotImplementedException)
            {
                Rippling.log.FlushOnScreen();
            }
        }

        [TestMethod]
        public void AnnotationsTest()
        {
            try
            {
                List<UnificationEntry> unifs = Markup.Annotate(new Expression(txt7, false), new Expression(txt8, false), false, MinimalityOption.NoRestrictions);
                //List<UnificationEntry> unifs = Markup.Annotate(new Expression(txt7,true), new Expression(txt8,true), false);
                Console.WriteLine(unifs.Count);
                int minimalCost = int.MaxValue;
                int count = 0;
                foreach (UnificationEntry UE in unifs)
                {
                    //if (UE.SubstCount == 0)
                    //{
                    count++;
                    if (UE.WRCost < minimalCost)
                        minimalCost = UE.WRCost;
                    //Console.WriteLine(UE.SubstCount + " " + UE.WRCost);
                    //}
                    Assert.IsTrue(UE.CompleteUnification() && Rippling.isSkeletonPreserving(UE.GivenRoot, UE.GoalRoot));
                }
                Console.WriteLine("Minimal cost: " + minimalCost);
            }
            catch (NotImplementedException)
            {
                Rippling.log.FlushOnScreen();
            }
        }

        [TestMethod]
        public void EqTest9and10()
        {
            Rippling.LoadRules("C:\\Users\\adm\\repos\\sempomat\\mathSearchWeb\\RuleCreator\\bin\\Debug\\rules");
            
            DateTime before = DateTime.Now;
            double result = Rippling.Eq(new Expression(txt9), new Expression(txt10), 0.85, true, true,MeasureComparisonMode.Shrunk);
            DateTime after = DateTime.Now;
            Console.WriteLine(result + " "+(after-before).TotalMilliseconds+" ms");
        }

        [TestMethod]
        public void EqTest9and10Invert()
        {
            Rippling.LoadRules("C:\\Users\\adm\\repos\\sempomat\\mathSearchWeb\\RuleCreator\\bin\\Debug\\rules");

            DateTime before = DateTime.Now;
            double result = Rippling.Eq(new Expression(txt9), new Expression(txt10), 0.85, true, true, MeasureComparisonMode.Invert);
            DateTime after = DateTime.Now;
            Console.WriteLine(result + " " + (after - before).TotalMilliseconds + " ms");
        }
        [TestMethod]
        public void EqTest10and9()
        {
            Rippling.LoadRules("C:\\Users\\adm\\repos\\sempomat\\mathSearchWeb\\RuleCreator\\bin\\Debug\\rules");
            DateTime before = DateTime.Now;
            double result = Rippling.Eq(new Expression(txt10), new Expression(txt9), 0.85, true, true, MeasureComparisonMode.Shrunk);
            DateTime after = DateTime.Now;
            Console.WriteLine(result + " " + (after - before).TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void EqTest10and12()
        {
            Rippling.LoadRules("C:\\Users\\adm\\repos\\sempomat\\mathSearchWeb\\RuleCreator\\bin\\Debug\\rules");
            DateTime before = DateTime.Now;
            double result = Rippling.Eq(new Expression(txt10), new Expression(txt12), 0.8, true, true, MeasureComparisonMode.Shrunk);
            DateTime after = DateTime.Now;
            Console.WriteLine(result + " " + (after - before).TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void EqTest10and12Invert()
        {
            Rippling.LoadRules("C:\\Users\\adm\\repos\\sempomat\\mathSearchWeb\\RuleCreator\\bin\\Debug\\rules");
            DateTime before = DateTime.Now;
            double result = Rippling.Eq(new Expression(txt10), new Expression(txt12), 0.8, true, true, MeasureComparisonMode.Invert);
            DateTime after = DateTime.Now;
            Console.WriteLine(result + " " + (after - before).TotalMilliseconds + " ms");
        }
    }
}
