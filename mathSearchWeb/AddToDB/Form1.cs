﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using mathsearch;
namespace AddToDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string ClearText(string text)
        {
            string result = "";
            text.Trim();
            text.ToLowerInvariant();
            bool isOpen = false;
            foreach (char c in text)
            {
                if (c == '<')
                    isOpen = true;
                if (c == '>')
                    isOpen = false;
                if (c == '\n' || c == '\r' || (!isOpen && c == ' '))
                    continue;
                result += c;
            }
            return result;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=Expressions;Integrated Security = True;");
            connection.Open();
            string c = ClearText(content.Text);
            Expression ex = new Expression(c);
            SqlCommand countCmd = new SqlCommand("SELECT COUNT(*) From List", connection);
            int count = (int)countCmd.ExecuteScalar() + 1;
            textBox1.Text = count.ToString();
            using (SqlCommand cmd = new SqlCommand("INSERT INTO List (expressionID, varCount, URL, Content, BinaryContent) VALUES (@keyValue, @intValue, @stringValue1, @stringValue2,@binaryValue)", connection))
            {
                cmd.Parameters.Add("@keyValue", SqlDbType.SmallInt).Value = count;
                cmd.Parameters.Add("@intValue", SqlDbType.SmallInt).Value = ex.varCount;
                cmd.Parameters.Add("@stringValue1", SqlDbType.NVarChar).Value = url.Text;
                cmd.Parameters.Add("@stringValue2", SqlDbType.NVarChar).Value = c;
                byte[] binaryContent = Expression.GetBinary(ex);
                cmd.Parameters.Add("@binaryValue", SqlDbType.VarBinary, binaryContent.Length).Value = binaryContent;
                cmd.ExecuteNonQuery();
            }
            connection.Close();
        }

        private void content_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
