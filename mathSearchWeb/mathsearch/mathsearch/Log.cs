﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace mathsearch
{
    public class Log
    {
        private StringBuilder messages;
        private string pathToJournal;
        public Log(string path)
        {
            messages = new StringBuilder();
            pathToJournal = path;
            Add("Journal init");
        }
        public void Add(string text)
        {
            messages.AppendLine("["+DateTime.Now.ToLongTimeString()+"]"+text);
        }
        public void Flush()
        {
            File.AppendAllText(pathToJournal, messages.ToString());
            messages.Clear();
        }
        public void FlushOnScreen()
        {
            Console.WriteLine(messages.ToString());
            messages.Clear();
        }
    }
}
