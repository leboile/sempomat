﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mathsearch
{
    [Serializable]
    public struct BinaryTreeInfo
    {
        public int SkeletonCount;
        public int WFCount;
        public int Depth;
        public int Total
        {
            get
            {
                return SkeletonCount + WFCount;
            }
        }
        public static BinaryTreeInfo operator +(BinaryTreeInfo op1, BinaryTreeInfo op2)
        {
            BinaryTreeInfo newStruct = new BinaryTreeInfo();
            newStruct.SkeletonCount = op1.SkeletonCount + op2.SkeletonCount;
            newStruct.WFCount = op1.WFCount + op2.WFCount;
            newStruct.Depth = op1.Depth;
            if (op2.Depth > op1.Depth)
                newStruct.Depth = op2.Depth;
            return newStruct;
        }
    }

    [Serializable()]
    public class BinaryTree
    {
        //[field: NonSerialized]
        //public event EventHandler TreeChangedEvent;
        //[field: NonSerialized]
        //private Expression expression;
        //public Expression Expr
        //{
        //    get
        //    {
        //        return expression;
        //    }
        //}
        private Element data;
        public Element Data
        {
            get
            {
                return data;
            }
            set
            {
                //if (data!=null)
                //    data.MetaMarkUpChangedEvent-=ElementMetaMarkUpChangedEventHandler;
                data = value;
                //data.MetaMarkUpChangedEvent += ElementMetaMarkUpChangedEventHandler;
                //RaiseTreeChangedEvent();
            }
        }
        public bool isCommutated;
        private BinaryTree left;
        private BinaryTree right;
        private BinaryTree parent;
        private int depth;
        public BinaryTree Left
        {
            get
            {
                if (left != null)
                    left.Parent = this;
                return left;
            }
            set
            {
                //if (left != null)
                //    left.TreeChangedEvent -= TreeChangedEventHandler;
                left = value;
                if (left != null)
                {
                    left.Parent = this;
                    //left.expression = expression;
                    //left.TreeChangedEvent += TreeChangedEventHandler;
                }
                //RaiseTreeChangedEvent();
            }
        }

        public BinaryTree Right
        {
            get
            {
                if (right != null)
                    right.Parent = this;
                return right;
            }
            set
            {
                //if (right != null)
                //   right.TreeChangedEvent -= TreeChangedEventHandler;
                right = value;
                if (right != null)
                {
                    right.Parent = this;
                    //right.expression = expression;
                    //right.TreeChangedEvent += TreeChangedEventHandler;
                }
                //RaiseTreeChangedEvent();
            }
        }

        public BinaryTree Root
        {
            get
            {
                BinaryTree p = this;
                while (p.Parent != null)
                    p = p.Parent;
                return p;
            }
        }
        public bool[] PathFromRoot
        {
            get
            {
                bool[] path = new bool[Depth];
                int i = Depth - 1;
                BinaryTree p = this;
                if (Parent == null)
                    return path;
                do
                {
                    path[i]=p.isRightNode;
                    p = p.Parent;
                    i--;
                } while (p.Parent != null);
                return path;
            }
        }
        public BinaryTree Parent
        {
            get
            {
                return parent;
            }
            set
            {
                //    if (parent != null)
                //        TreeChangedEvent -= parent.TreeChangedEventHandler;
                parent = value;
                //if (parent != null)
                //    TreeChangedEvent += parent.TreeChangedEventHandler;
                //RaiseTreeChangedEvent();
            }
        }

        public BinaryTree Sibling
        {
            get
            {
                if (Parent != null)
                    return isLeftNode ? Parent.Right : Parent.Left;
                else
                    return null;
            }
        }

        public bool isLeftNode
        {
            get
            {
                if (Parent != null)
                    return Object.ReferenceEquals(Parent.Left, this);
                else
                    return false;
            }
        }

        public bool isRightNode
        {
            get
            {
                if (Parent != null)
                    return Object.ReferenceEquals(Parent.Right, this);
                else
                    return false;
            }
        }

        public int Depth
        {
            get
            {
                if (depth == -1)
                {
                    int res = 0;
                    BinaryTree t = this.Parent;
                    if (t != null)
                        res = t.Depth + 1;
                    return res;
                }
                else
                    return depth;
            }
        }

        public BinaryTree First//first symbol
        {
            get
            {
                BinaryTree res = this;
                while (res.Left != null || res.Right != null)
                {
                    if (res.Left != null)
                        res = res.Left;
                    else
                        res = res.Right;
                }
                return res;
            }
        }

        public BinaryTree Next//get next node in lexicographical order
        {
            get
            {
                if (Right == null)
                {
                    if (Parent != null && Element.ReferenceEquals(Parent.Left, this))
                        return Parent;
                    else
                    {
                        BinaryTree node = this;
                        while (node.Parent != null && !Element.ReferenceEquals(node.Parent.Left, node))
                        {
                            node = node.Parent;
                        }
                        //if (node.Parent.Parent == null && node.Parent.isRightNode)
                        //    return null;
                        return node.Parent;
                    }
                }
                else
                {
                    BinaryTree node = this.Right;
                    while (node.Left != null)
                    {
                        node = node.Left;
                    }
                    return node;
                }
            }
        }


        public BinaryTree(Element D)
        {
            if (D == null)
                throw new ArgumentNullException("D");
            Data = D.Copy();
            left = null;
            right = null;
            parent = null;
            depth = -1;
            //expression = null;
            isCommutated = false;
            //D.MetaMarkUpChangedEvent += ElementMetaMarkUpChangedEventHandler;
        }

        public BinaryTree(Element D, Expression e)
        {
            if (D == null)
                throw new ArgumentNullException("D");
            Data = D.Copy();
            left = null;
            right = null;
            parent = null;
            depth = -1;
            //expression = e;
            //expression = null;
            isCommutated = false;
            //D.MetaMarkUpChangedEvent += ElementMetaMarkUpChangedEventHandler;
        }

        public void ReplaceCurrentNodeWith(BinaryTree newNode)
        {
            if (newNode == null)
                return;
            this.Data = newNode.Data.Copy();
            if (this.Left != null)
                this.Left.Parent = null;
            if (this.Right != null)
                this.Right.Parent = null;
            this.Left = null;
            this.Right = null;
            this.Left = newNode.Left;
            this.Right = newNode.Right;
        }

        public void ReplaceSpecificNodes(Element target, BinaryTree newNode, bool keepMarkup)
        {
            if (target.type != ElementType.Variable)
                throw new NotImplementedException("Target element is not variable "+target+" a"+newNode.Data+Object.ReferenceEquals(target, newNode.Data));
            if (newNode == null)
                return;
            List<BinaryTree> result = new List<BinaryTree>();
            FindAll(target, ref result);
            foreach (BinaryTree t in result)
            {
                BinaryTree substTree = newNode.DeepCopy();
                if (keepMarkup)
                    Markup.SetMarkup(substTree, t.Data.direction);
                t.ReplaceCurrentNodeWith(substTree);
            }
        }

        public BinaryTree DeepCopy()
        {
            BinaryTree newTree = new BinaryTree(data.Copy(), null);
            newTree.parent = parent;
            newTree.isCommutated = isCommutated;
            if (Left != null)
                newTree.Left = Left.DeepCopy();
            if (Right != null)
                newTree.Right = Right.DeepCopy();
            return newTree;
        }

        public BinaryTree DeepCopy(Expression e)
        {
            BinaryTree newTree = new BinaryTree(data, e);
            newTree.parent = parent;
            newTree.isCommutated = isCommutated;
            if (Left != null)
                newTree.Left = Left.DeepCopy(e);
            if (Right != null)
                newTree.Right = Right.DeepCopy(e);
            return newTree;
        }

        public BinaryTree DeepCopyWholeTree(out BinaryTree rootCopy)//returns corresponding node of the copy
        {
            bool[] path = PathFromRoot;//false - for left, true - for right
            BinaryTree p = this;
            if (p.Parent == null)
            {
                rootCopy = p.DeepCopy();
                return rootCopy;
            }
            rootCopy = p.Root.DeepCopy();
            //if (copy != this)
            //    throw new NotImplementedException("Algorithm is incorrect");
            return rootCopy.NodeByPath(path);
        }

        public int Add(Element D)
        {
            if (this.Data == null || (this.Data.type != ElementType.Function && this.Data.type != ElementType.ServiceObject))
                throw new InvalidOperationException("Element is null or has a wrong type");
            if (left == null)
            {
                Left = new BinaryTree(D);
                return -1;
            }
            else
                if (right == null)
                {
                    Right = new BinaryTree(D);
                    return 1;
                }
            return 0;
        }

        public int Add(BinaryTree t)
        {
            if (this.Data == null || (this.Data.type != ElementType.Function && this.Data.type != ElementType.ServiceObject))
                throw new InvalidOperationException("Element is null or has a wrong type");
            if (left == null)
            {
                Left = t;
                return -1;
            }
            else
                if (right == null)
                {
                    Right = t;
                    return 1;
                }
            return 0;
        }

        public BinaryTree GuaranteedAdd(BinaryTree t)//returns parent of the added node
        {
            if (this.Data == null || (this.Data.type != ElementType.Function && this.Data.type != ElementType.ServiceObject))
                throw new InvalidOperationException("Element is null or has a wrong type");
            if (left == null)
            {
                Left = t;
                return this;
            }
            else
                if (right == null)
                {
                    Right = t;
                    return this;
                }
                else
                {
                    BinaryTree rightBackup = right;
                    right = new BinaryTree(Data);
                    right.Left = rightBackup;
                    right.Right = t;
                    return this.right;
                }
        }

        public BinaryTree GuaranteedAdd(Element t)//returns added node
        {
            if (left == null)
            {
                Left = new BinaryTree(t);
                return Left;
            }
            else
                if (right == null)
                {
                    Right = new BinaryTree(t);
                    return Right;
                }
                else
                {
                    BinaryTree rightBackup = right;
                    right = new BinaryTree(Data);
                    right.Left = rightBackup;
                    right.Right = new BinaryTree(t);
                    return this.right.right;
                }
        }

        public BinaryTree Find(Element obj)
        {
            BinaryTree res = null;
            if (Data == obj)
                res = this;
            else
            {
                if (Left != null)
                    res = Left.Find(obj);
                if (Right != null && res == null)
                    res = Right.Find(obj);
            }
            return res;
        }

        public BinaryTree Find(Element obj, MarkupType dir)
        {
            BinaryTree res = null;
            if (Data == obj && Data.direction == dir)
                res = this;
            else
            {
                if (Left != null)
                    res = Left.Find(obj, dir);
                if (Right != null && res == null)
                    res = Right.Find(obj, dir);
            }
            return res;
        }

        public void FindAll(Element obj, ref List<BinaryTree> result)
        {
            if (obj == null)
                return;
            if (Data == obj)
                result.Add(this);
            if (Left != null)
                Left.FindAll(obj, ref result);
            if (Right != null)
                Right.FindAll(obj, ref result);
        }

        public BinaryTree Find(Element obj, Dictionary<Element, BinaryTree> substTable)
        {
            BinaryTree res = null;
            if (Data == obj)
                res = this;
            else
            {
                BinaryTree sub = Rippling.GetLastSubst(this, substTable);
                if (!ReferenceEquals(sub, this))
                    res = sub.Find(obj, substTable);
                if (Left != null && res == null)
                    res = Left.Find(obj, substTable);
                if (Right != null && res == null)
                    res = Right.Find(obj, substTable);
            }
            return res;
        }

        private void print(int offset)
        {
            for (int i = 0; i < offset; i++)
                Console.Write(" ");
            Console.WriteLine(Data);
            if (Left != null)
                Left.print(offset + 1);
            if (Right != null)
                right.print(offset + 1);
        }

        public string PlainText()
        {
            StringBuilder SB = new StringBuilder();
            BinaryTree node = this.First;
            while (node != null)
            {
                if (node.Data == null)
                {
                    node = node.Next;
                    continue;
                }
                if (node.Data.direction == MarkupType.Skeleton)
                    SB.Append(node.Data.name + " ");
                else
                    SB.Append("!" + node.data.name + "! ");
                node = node.Next;
            }
            return SB.ToString();
        }

        public override bool Equals(object obj)
        {
            BinaryTree t = obj as BinaryTree;
            if (t == null)
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            return checkStructure(this, t);
        }

        public static bool checkStructure(BinaryTree t1, BinaryTree t2)
        {
            if (t1 == null && t2 == null)
                return true;
            if ((t1 == null || t2 == null) || (t1.Data != t2.Data))
                return false;
            return (checkStructure(t1.Left, t2.Left) && checkStructure(t1.Right, t2.Right)) || (checkStructure(t1.Right, t2.Left) && checkStructure(t1.Left, t2.Right));
        }

        private void printToString(int offset, StringBuilder SB)
        {
            for (int i = 0; i < offset; i++)
                SB.Append(" ");
            SB.AppendLine(Data.ToString());
            if (Left != null)
                Left.printToString(offset + 1, SB);
            if (Right != null)
                Right.printToString(offset + 1, SB);
        }

        public override string ToString()
        {
            StringBuilder SB = new StringBuilder();
            SB.AppendLine(Data.ToString());
            if (Left != null)
                Left.printToString(1, SB);
            if (Right != null)
                Right.printToString(1, SB);
            return SB.ToString();
        }

        public static bool operator ==(BinaryTree obj1, BinaryTree obj2)
        {
            if ((object)obj1 == null && (object)obj2 == null)
                return true;
            else
                if ((object)obj1 != null && (object)obj2 != null)
                    return obj1.Equals(obj2);
                else
                    return false;
        }

        public static bool operator !=(BinaryTree obj1, BinaryTree obj2)
        {
            return !(obj1 == obj2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static void DeleteWaveFronts(BinaryTree t)
        {
            if (t == null)
                return;
            if (t.Data.direction != MarkupType.Skeleton)
                t.Data.direction = MarkupType.Skeleton;
            DeleteWaveFronts(t.Left);
            DeleteWaveFronts(t.Right);
        }

        public HashSet<Variable> GetAllVariables()
        {
            HashSet<Variable> result = new HashSet<Variable>();
            if (this.Data.type == ElementType.Variable)
                result.Add((Variable)this.Data.Copy());
            if (Left != null)
                result.UnionWith(Left.GetAllVariables());
            if (Right != null)
                result.UnionWith(Right.GetAllVariables());
            return result;
        }

        public BinaryTreeInfo GetTreeInfo()
        {
            BinaryTreeInfo res = new BinaryTreeInfo();
            if (this.Data.direction == MarkupType.Skeleton)
                res.SkeletonCount++;
            else
                res.WFCount++;
            if (Left != null)
                res += Left.GetTreeInfo();
            if (Right != null)
                res += Right.GetTreeInfo();
            res.Depth++;//adding this actual node
            return res;
        }

        public BinaryTree NodeByPath(ICollection<bool> path)
        {
            if (path == null)
                return null;
            BinaryTree node = this;
            try
            {
                foreach (bool step in path)
                {
                    if (step)
                        node = node.Right;
                    else
                        node = node.Left;
                }
            }
            catch (NullReferenceException)
            {
                return null;
            }
            return node;
        }

        //protected virtual void RaiseTreeChangedEvent()
        //{
        //    // Make a temporary copy of the event to avoid possibility of
        //    // a race condition if the last subscriber unsubscribes
        //    // immediately after the null check and before the event is raised.
        //    EventHandler handler = TreeChangedEvent;

        //    // Event will be null if there are no subscribers
        //    if (handler != null)
        //    {
        //        // Use the () operator to raise the event.
        //        handler(this,new EventArgs());
        //    }
        //}

        //void ElementMetaMarkUpChangedEventHandler(object sender, EventArgs e)
        //{
        //    RaiseTreeChangedEvent();
        //}
        //void TreeChangedEventHandler(object sender, EventArgs e)
        //{
        //    RaiseTreeChangedEvent();
        //}
    }
}
