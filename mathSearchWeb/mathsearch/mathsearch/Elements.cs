﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mathsearch
{
    public enum ElementType
    {
        ServiceObject,
        Function,
        Variable,
        Constant
    }
    [Serializable()]
    public abstract class Element
    {
        //[field: NonSerialized]
        //public event EventHandler MetaMarkUpChangedEvent;
        public string name;
        public ElementType type;
        public MarkupType direction = MarkupType.Undefined;
        public override bool Equals(object obj)
        {
            Element o = obj as Element;
            if (o == null)
                return false;
            return (name == o.name) && (type == o.type);
        }
        public static bool operator ==(Element obj1, Element obj2)
        {
            //if ((object)obj1 == null || (object)obj2 == null)
            //    return false;
            //return obj1.Equals(obj2);
            if ((object)obj1 == null && (object)obj2 == null)
                return true;
            else
                if ((object)obj1 != null && (object)obj2 != null)
                    return obj1.Equals(obj2);
                else
                    return false;
        }

        public static bool operator !=(Element obj1, Element obj2)
        {
            return !(obj1 == obj2);
        }

        public override int GetHashCode()//it is used in dictionary for example while doing trygetvalue
        {
            return (name + " " + type.ToString()).GetHashCode();//good enough
            //return base.GetHashCode();
        }
        public Element Copy()
        {
            switch (type)
            {
                case ElementType.Constant:
                    return new Constant(name, direction);
                case ElementType.Function:
                    return new Function(name, direction);
                case ElementType.ServiceObject:
                    return new ServiceObject(name, direction);
                case ElementType.Variable:
                    return new Variable(name, direction);
                default:
                    return null;
            }
        }
        public override string ToString()
        {
            return name + " " + direction;
        }

        //private void RaiseMetaMarkUpChangedEvent()
        //{
        //    // Make a temporary copy of the event to avoid possibility of
        //    // a race condition if the last subscriber unsubscribes
        //    // immediately after the null check and before the event is raised.
        //    EventHandler handler = MetaMarkUpChangedEvent;
        //    // Event will be null if there are no subscribers
        //    if (handler != null)
        //    {
        //        // Use the () operator to raise the event.
        //        handler(this, new EventArgs());
        //    }
        //}
    }

    [Serializable()]
    public class ServiceObject : Element
    {
        public ServiceObject(string n)
        {
            name = n.Trim().ToLower();
            type = ElementType.ServiceObject;
        }
        public ServiceObject(string n, MarkupType dir)
        {
            name = n.Trim().ToLower();
            type = ElementType.ServiceObject;
            direction = dir;
        }
    }

    [Serializable()]
    public class Function : Element
    {
        private static string[] commutativeFunctions = { "plus", "times"};
        private bool commutative;
        public bool Commutative
        {
            get
            {
                return commutative;
            }
        }
        public Function(string n)
        {
            name = n.Trim().ToLower();
            type = ElementType.Function;
            if (commutativeFunctions.Contains(name))
                commutative = true;
            else
                commutative = false;
        }

        public Function(string n, MarkupType dir)
        {
            name = n.Trim().ToLower();
            type = ElementType.Function;
            direction = dir;
            if (commutativeFunctions.Contains(name))
                commutative = true;
            else
                commutative = false;
        }
    }

    [Serializable()]
    public class Variable : Element
    {
        public Variable(string n)
        {
            name = n.Trim();
            type = ElementType.Variable;
        }
        public Variable(string n, MarkupType dir)
        {
            name = n.Trim();
            type = ElementType.Variable;
            direction = dir;
        }
    }

    [Serializable()]
    public class Constant : Element
    {
        public Constant(string n)
        {
            name = n.Trim();
            type = ElementType.Constant;
        }
        public Constant(string n, MarkupType dir)
        {
            name = n.Trim();
            type = ElementType.Constant;
            direction = dir;
        }
    }
}
