﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
namespace mathsearch
{
    public enum MinimalityOption
    {
        NoRestrictions,
        CostMinimal,
        SubstMinimal,
        CostAndSubstMinimal
    }
    internal class DUNIFYMiscellanious
    {
        private List<UnificationEntry> results;
        public List<UnificationEntry> Results
        {
            get
            {
                return results;
            }
        }
        private bool costMinimal;
        public bool CostMinimal
        {
            get
            {
                return costMinimal;
            }
        }
        private bool substMinimal;
        public bool SubstMinimal
        {
            get
            {
                return substMinimal;
            }
        }
        private bool finishUnificationFlag;
        public bool IsUnificationFinished
        {
            get
            {
                return finishUnificationFlag;
            }
        }
        public void FinishUnification()
        {
            finishUnificationFlag = true;
        }

        private bool useConveyor;
        public bool ProceedWithRippling
        {
            get
            {
                return useConveyor;
            }
        }
        private bool forceDD;
        public bool ForcedDECOMPOSEDELETE
        {
            get
            {
                return forceDD;
            }
        }
        private bool minimalUnificationComplete;
        internal bool MinimalUnificationComplete
        {
            get
            {
                return minimalUnificationComplete;
            }
            set
            {
                minimalUnificationComplete = value;
            }
        }
        private int minimalUnificationSubstCount;
        internal int MinimalUnificationSubstCount
        {
            get
            {
                return minimalUnificationSubstCount;
            }
            set
            {
                minimalUnificationSubstCount = value;
            }
        }
        private int minimalUnificationCost;
        internal int MinimalUnificationCost
        {
            get
            {
                return minimalUnificationCost;
            }
            set
            {
                minimalUnificationCost = value;
            }
        }
        public DUNIFYMiscellanious(bool furtherRippling, MinimalityOption minimal, bool forcedd)
        {
            forceDD = forcedd;
            useConveyor = furtherRippling;
            finishUnificationFlag = false;
            switch (minimal)
            {
                case MinimalityOption.NoRestrictions:
                    substMinimal = false;
                    costMinimal = false;
                    break;
                case MinimalityOption.CostMinimal:
                    break;
                case MinimalityOption.SubstMinimal:
                    substMinimal = true;
                    costMinimal = false;
                    break;
                case MinimalityOption.CostAndSubstMinimal:
                    substMinimal = true;
                    costMinimal = true;
                    break;
            }
            minimalUnificationComplete = false;
            minimalUnificationSubstCount = int.MaxValue;
            minimalUnificationCost = int.MaxValue;
            results = new List<UnificationEntry>();
        }
    }

    public class UnificationEntry
    {
        private int similaritiesCount;//e.g. DECOMPOSE and DELETE counter
        public int SimiliarCount
        {
            get
            {
                return similaritiesCount;
            }
        }
        private int substitutionsCount;//e.g. ELIMINATE and IMITATE counter
        public int SubstCount
        {
            get
            {
                return substitutionsCount;
            }
        }
        private int cost;//HIDE counter
        public int WRCost
        {
            get
            {
                return cost;
            }
        }
        internal void IncreaseSimilarCount()
        {
            similaritiesCount++;
        }

        internal void IncreaseSubstCount()
        {
            substitutionsCount++;
        }

        internal void IncreaseCost()
        {
            cost++;
        }

        private bool unificationFinished;
        public bool UnificationFinished
        {
            get
            {
                return unificationFinished;
            }
        }
        private BinaryTree givenRoot;
        public BinaryTree GivenRoot
        {
            get
            {
                return givenRoot;
            }
        }

        private BinaryTree goalRoot;
        public BinaryTree GoalRoot
        {
            get
            {
                return goalRoot;
            }
        }

        private BinaryTree goalNode;
        internal BinaryTree GoalNode
        {
            get
            {
                return goalNode;
            }
            set
            {
                goalNode = value;
            }
        }

        private BinaryTree givenNode;
        internal BinaryTree GivenNode
        {
            get
            {
                return givenNode;
            }
            set
            {
                givenNode = value;
            }
        }

        private Measure m;
        public Measure CommonMeasure
        {
            get
            {
                return m;
            }
        }

        private bool alreadyRippled;

        private Skeleton givenSkeleton;
        private Skeleton goalSkeleton;

        public bool CompleteUnification()
        {
            if (unificationFinished)
                return true;
            Markup.SetMarkup(givenRoot, MarkupType.Skeleton, MarkupType.Undefined);
            Markup.SetMarkup(goalRoot, MarkupType.Skeleton, MarkupType.Undefined);
            givenSkeleton = new Skeleton(GivenRoot);
            goalSkeleton = new Skeleton(GoalRoot);
            Measure m1 = new Measure(GivenRoot);
            Measure m2 = new Measure(GoalRoot);
            try
            {
                if (givenSkeleton == goalSkeleton)
                {
                    m = m1 + m2;
                    unificationFinished = true;
                    return true;
                }
                else
                {
                    Markup.SetMarkup(givenRoot, MarkupType.Undefined, MarkupType.Skeleton);
                    Markup.SetMarkup(goalRoot, MarkupType.Undefined, MarkupType.Skeleton);
                    return false;
                }
            }
            catch (InvalidOperationException)
            {
                Rippling.log.Add("M1: " + m1);
                Rippling.log.Add("t1: ");
                Rippling.log.Add(givenRoot.Root.ToString());
                Rippling.log.Add("M2: " + m2);
                Rippling.log.Add("t2: ");
                Rippling.log.Add(goalRoot.Root.ToString());
                Rippling.log.Add(m1 + " " + m2 + " Skeleton preservance: " + Rippling.isSkeletonPreserving(givenRoot, goalRoot).ToString());
                Markup.SetMarkup(givenRoot, MarkupType.Undefined, MarkupType.Skeleton);
                Markup.SetMarkup(goalRoot, MarkupType.Undefined, MarkupType.Skeleton);
                return false;
            }
        }

        internal void StartRippling(DUNIFYMiscellanious misc)
        {
            if (CompleteUnification() && !alreadyRippled && !misc.IsUnificationFinished)
            {
                //threading goes here
                alreadyRippled = true;
                Rippling.Magazine.AddToQueue(this);
            }
            else
            {
                return;
            }
            //throw new ArgumentException("Invalid unification entry", "e");

            if (ThreadMagazine.maxWeight > Rippling.Limit && !misc.IsUnificationFinished)
            {
                Rippling.log.Add("Got >" + Rippling.Limit.ToString() + " stopping annotation proccess...");
                misc.FinishUnification();
            }
        }


        internal static void NewUnificationEntryHandler(object param)
        {
            //try
            //{
            UnificationEntry e = param as UnificationEntry;
            if (e == null)
                throw new ArgumentException("Wrong parameter NewUnificationEntryHandler", "param");
            RipplingStepResult ripplingResult = new RipplingStepResult(0.1);
            Rippling.ripplingStep(e.GivenRoot, e.GoalRoot, e.GivenRoot.DeepCopy(), e.GoalRoot.DeepCopy(), e.CommonMeasure, ref ripplingResult);
            //Console.WriteLine("Rules applied: " + ResultInfo.numberOfRulesAppliedStorage[ripplingResult.storageIndex]);
            Rippling.Magazine.AddResult(ripplingResult.MaxWeight);
            //}
            //catch (Exception ex)
            //{
            //    log.Add("RipplingStep launch exception "+ex.Message);
            //}
        }

        //internal UnificationEntry Left()
        //{
        //    UnificationEntry e = new UnificationEntry(givenRoot, goalRoot);
        //    e.givenNode = givenNode.Left;
        //    e.goalNode = goalNode.Left;
        //    e.cost = cost;
        //    e.substitutionsCount = substitutionsCount;
        //    e.similaritiesCount = similaritiesCount;
        //    e.unificationFinished = unificationFinished;
        //    return e;
        //}

        //internal UnificationEntry Right()
        //{
        //    UnificationEntry e = new UnificationEntry(givenRoot, goalRoot);
        //    e.givenNode = givenNode.Right;
        //    e.goalNode = goalNode.Right;
        //    e.cost = cost;
        //    e.substitutionsCount = substitutionsCount;
        //    e.similaritiesCount = similaritiesCount;
        //    e.unificationFinished = unificationFinished;
        //    return e;
        //}

        internal UnificationEntry MoveByPath(bool[][] path)
        {
            givenNode = givenRoot.NodeByPath(path[0]);
            goalNode = GoalRoot.NodeByPath(path[1]);
            return this;
        }

        internal UnificationEntry(UnificationEntry entry)
        {
            goalNode = entry.goalNode.DeepCopyWholeTree(out goalRoot);
            givenNode = entry.givenNode.DeepCopyWholeTree(out givenRoot);
            cost = entry.WRCost;
            substitutionsCount = entry.SubstCount;
            similaritiesCount = entry.SimiliarCount;
            unificationFinished = entry.unificationFinished;
            alreadyRippled = entry.alreadyRippled;
        }

        public UnificationEntry(BinaryTree tree, BinaryTree target)
        {
            similaritiesCount = 0;
            substitutionsCount = 0;
            cost = 0;
            unificationFinished = false;
            alreadyRippled = false;
            givenRoot = tree;
            goalRoot = target;
            givenNode = givenRoot;
            goalNode = goalRoot;
        }

        public override string ToString()
        {
            StringBuilder SB = new StringBuilder();
            SB.AppendLine("Given: ");
            SB.AppendLine(GivenRoot.ToString());
            SB.AppendLine("Goal: ");
            SB.AppendLine(GoalRoot.ToString());
            return SB.ToString();
        }
    }

    internal class ThreadMagazine//part of rippling conveyor, ThreadMagazine responsible for multithreading
    {
        public static Double maxWeight = Double.MinValue;
        private static List<double> resultsList = new List<double>();
        private int maxThreads;
        private int currThreads;
        private Thread[] threads;
        private List<UnificationEntry> queue;
        private bool isFinished;
        private int totalProcessed;
        private int totalQueued;
        private bool clipMaximal;
        public ThreadMagazine(int maxthreads, bool clipmaximal)
        {
            if (maxthreads < 1)
                throw new ArgumentException("Arg maxthreads less than 1", "maxthreads");
            clipMaximal = clipmaximal;
            maxThreads = maxthreads;
            currThreads = maxThreads / 10 + 1;
            threads = new Thread[maxThreads];
            for (int i = 0; i < maxThreads; i++)
                threads[i] = new Thread(new ParameterizedThreadStart(UnificationEntry.NewUnificationEntryHandler));
            isFinished = false;
            maxWeight = Double.MinValue;
            resultsList = new List<double>();
            queue = new List<UnificationEntry>();
            totalProcessed = 0;
            totalQueued = 0;
        }

        public void AddToQueue(UnificationEntry e)
        {
            if (isFinished)
                return;
            totalQueued++;
            int WFCount = e.CommonMeasure.WavefrontCount;
            string givenPlainText = e.GivenRoot.PlainText();
            string goalPlainText = e.GoalRoot.PlainText();
            for (int i = 0; i < queue.Count; i++)
            {
                int QueueEntryWFCount = queue[i].CommonMeasure.WavefrontCount;
                if (QueueEntryWFCount > WFCount)
                {
                    queue.Insert(i, e);
                    RefreshQueue();
                    return;
                }
                //else//seeems to be useless
                //    if (QueueEntryWFCount == WFCount && queue[i].given.PlainText() == givenPlainText && queue[i].goal.PlainText() == goalPlainText)
                //    {
                //        return;
                //    }
            }
            if (clipMaximal && queue.Count > 0)//clipping of current maximal unification
            {
                RefreshQueue();
                return;
            }
            queue.Add(e);//adds to the end of the queue if there is no appropriate place within queue or if queue is empty
            RefreshQueue();
        }

        public void Finish(bool waitForQueue)
        {
            while (true && waitForQueue)
                if (queue.Count == 0)
                    break;
                else
                    RefreshQueue();
            isFinished = true;
            queue.Clear();
            totalProcessed = 0;
            totalQueued = 0;
            for (int i = 0; i < currThreads; i++)
            {
                try
                {
                    threads[i].Abort();
                }
                catch (ThreadStateException ex)
                {
                    Rippling.log.Add(ex.Message);
                }
                catch (ThreadInterruptedException TIE)
                {
                    Rippling.log.Add(TIE.Message);
                }
            }
        }

        private void RefreshQueue()//while queue is not "lock" this method should be called only by main thread strictly
        {
            if (isFinished || queue.Count == 0)
                return;

            if (queue.Count / 50 > currThreads && currThreads < maxThreads)
            {
                currThreads++;
                Rippling.log.Add("AMOUNT OF THREADS INCREASED BY ONE");
            }

            for (int i = 0; i < currThreads; i++)
            {
                if (!threads[i].IsAlive)
                {
                    //lock (queue)
                    //{
                    if (queue.Count > 0)
                    {
                        threads[i] = new Thread(new ParameterizedThreadStart(UnificationEntry.NewUnificationEntryHandler));
                        threads[i].Start(queue[0]);
                        queue.RemoveAt(0);
                    }
                    //}
                }
                else
                {
                    switch (threads[i].ThreadState)
                    {
                        case ThreadState.WaitSleepJoin:
                            threads[i].Interrupt();
                            //RefreshQueue();
                            break;
                        case ThreadState.AbortRequested:
                            try
                            {
                                //threads[i].join
                            }
                            catch (ThreadStateException ex)
                            {
                                Rippling.log.Add(ex.Message);
                            }
                            catch (ThreadInterruptedException TIE)
                            {
                                Rippling.log.Add(TIE.Message);
                            }
                            //RefreshQueue();
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public void AddResult(double res)
        {
            if (isFinished)
                return;
            totalProcessed++;
            lock (resultsList)
            {
                resultsList.Add(res);
                if (res > maxWeight)
                    maxWeight = res;
                //calculations with resultsList to implement "smart" detecting of succesfull rippling result
                int counter = 0;
                foreach (double d in resultsList)
                    if (d > 0.95 * Rippling.Limit)
                        counter++;
                if (maxWeight < Rippling.Limit && counter > resultsList.Count / 3)
                    maxWeight = maxWeight + 2 * (Rippling.Limit - maxWeight);
                //Rippling.log.Add(res.ToString() + " Processed: " + totalProcessed + " and in queue " + queue.Count.ToString() + " currThreads: " + currThreads + " limit: " + Rippling.Limit);
                //Rippling.log.FlushOnScreen();
                //RefreshQueue();// Dangerouse 'cause threads could ran into race condition, while taking another item from the queue
                //and if queue consists of only one element then one thread could take it and another would execute .Remove(0) when queue is empty
            }
        }
    }

    internal class RipplingStepResult
    {
        private double maxWeight;
        private double alpha;
        private int num;
        private bool isFinished;
        public int NumberOfRulesApplied
        {
            get
            {
                return num;
            }
            set
            {
                num = value;
            }
        }
        public bool IsFinished
        {
            get
            {
                return isFinished;
            }
        }
        public double MaxWeight
        {
            get
            {
                return maxWeight;
            }
        }
        public RipplingStepResult(double a)
        {
            maxWeight = double.MinValue;
            alpha = a;
            num = 0;
            isFinished = false;
        }

        public void AddResult(BinaryTree given, BinaryTree goal, bool isEqual)
        {
            if (isEqual)
            {
                maxWeight = 1.0;
                isFinished = true;
            }
            else
            {
                BinaryTreeInfo givenInfo = given.GetTreeInfo();
                BinaryTreeInfo goalInfo = goal.GetTreeInfo();
                double weight = (givenInfo.SkeletonCount + goalInfo.SkeletonCount) / (double)(givenInfo.Total + goalInfo.Total + alpha * num);
                if (weight > maxWeight)
                {
                    //if (maxWeight==double.MinValue)
                    //Console.WriteLine("NEWMAXWEIGHT PREVIOUS: MINDOUBLE" +" NOW: "+weight.ToString("F"));
                    //else
                    //    Console.WriteLine("NEWMAXWEIGHT PREVIOUS: " + maxWeight.ToString("F") + " NOW: " + weight.ToString("F"));
                    maxWeight = weight;
                }
            }
        }
    }
}
