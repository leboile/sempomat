﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace mathsearch
{
    [Serializable]
    public class Expression
    {
        public static int totalExprCount = 0;
        public int varCount = 0;
        public List<string> vars = new List<string>(); //names of variables
        private BinaryTree treeRoot;
        public BinaryTree Root
        {
            get
            {
                return treeRoot;
            }
        }
        private Skeleton skeleton;
        public Skeleton Skeleton
        {
            get
            {
                if (skeleton == null)
                    skeleton = new Skeleton(treeRoot);
                return skeleton;
            }
        }
        bool isWaveRule;
        private Expression()
        {
            varCount = 0;
            treeRoot = null;
            totalExprCount++;
        }

        public Expression(XmlNode root, bool isWR)
        {
            treeRoot = new BinaryTree(new ServiceObject(root.Name));
            isWaveRule = isWR;
            buildTree(treeRoot, root);
            treeRoot = treeRoot.Left;
            treeRoot.Parent = null;
            totalExprCount++;
            //treeRoot.TreeChangedEvent += TreeChangedEventHandler;
        }

        public Expression(string content)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(content);
            XmlNode root = doc.DocumentElement;
            treeRoot = new BinaryTree(new ServiceObject(root.Name));
            isWaveRule = false;
            buildTree(treeRoot, root);
            treeRoot = treeRoot.Left;
            treeRoot.Parent = null;
            totalExprCount++;
            //treeRoot.TreeChangedEvent += TreeChangedEventHandler;
        }

        public Expression(string content, bool IsWR)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(content);
            XmlNode root = doc.DocumentElement;
            treeRoot = new BinaryTree(new ServiceObject(root.Name));
            isWaveRule = IsWR;
            buildTree(treeRoot, root);
            treeRoot = treeRoot.Left;
            treeRoot.Parent = null;
            totalExprCount++;
            //treeRoot.TreeChangedEvent += TreeChangedEventHandler;
        }

        public Expression(BinaryTree t)
        {
            treeRoot = t;
            isWaveRule = false;
            HashSet<Variable> v = treeRoot.GetAllVariables();
            foreach (Variable var in v)
                vars.Add(var.name);
            //treeRoot.TreeChangedEvent += TreeChangedEventHandler;
            totalExprCount++;
        }

        public Expression DeepCopy()
        {
            Expression copy = new Expression();
            copy.varCount = varCount;
            copy.treeRoot = treeRoot.DeepCopy(copy);
            //copy.treeRoot.TreeChangedEvent += TreeChangedEventHandler;
            foreach (string v in vars)
                copy.vars.Add(v);
            return copy;
        }

        //private void TreeChangedEventHandler(object sender, EventArgs e)
        //{
        //    skeleton = null;
        //}

        private void getVarCount(BinaryTree t)
        {
            if (t == null)
                return;
            if (t.Data.type == ElementType.Variable)
            {
                int ind = vars.IndexOf(t.Data.name);
                if (ind == -1)
                {
                    varCount++;
                    vars.Add(t.Data.name);
                }
            }
            getVarCount(t.Left);
            getVarCount(t.Right);
        }

      
        void buildTree(BinaryTree t, XmlNode node)
        {
            switch (node.NodeType)
            {
                case XmlNodeType.Element:
                    int res = 0;
                    switch (node.Name.ToLower())
                    {
                        case "set":
                            res = t.Add(new Function("set"));
                            if (node.HasChildNodes)
                            {
                                if (res == -1)
                                    buildTree(t.Left, node.FirstChild);
                                else
                                    if (res == 1)
                                        buildTree(t.Right, node.FirstChild);
                                    else
                                        throw new Exception("Tree overflow");
                            }
                            break;
                        case "apply":
                            //node = node.FirstChild;
                            res = t.Add(new Function(node.FirstChild.Name));
                            if (node.HasChildNodes)
                            {
                                if (res == -1)
                                    buildTree(t.Left, node.FirstChild);
                                else
                                    if (res == 1)
                                        buildTree(t.Right, node.FirstChild);
                                    else
                                        throw new Exception("Tree overflow");
                            }
                            break;
                        case "ci":
                            int ind = vars.IndexOf(node.InnerText);
                            if (ind < 0)
                            {
                                if (isWaveRule)
                                {
                                    t.Add(new Variable("WAVERULEVAR" + node.InnerText));
                                }
                                else
                                    t.Add(new Variable(node.InnerText + totalExprCount + varCount));
                                varCount++;
                                vars.Add(node.InnerText);
                            }
                            else
                            {
                                if (isWaveRule)
                                    t.Add(new Variable("WAVERULEVAR" + node.InnerText));
                                else
                                    t.Add(new Variable(node.InnerText + totalExprCount + ind));
                            }
                            break;
                        case "cn":
                            t.Add(new Constant(node.InnerText));
                            break;
                        case "math":
                            if (node.HasChildNodes)
                                buildTree(t, node.FirstChild);
                            break;
                        default:
                            if (node.HasChildNodes)
                                buildTree(t, node.FirstChild);
                            break;
                    }
                    break;
                case XmlNodeType.Text:
                case XmlNodeType.XmlDeclaration:
                case XmlNodeType.ProcessingInstruction:
                case XmlNodeType.Comment:
                case XmlNodeType.EndElement:
                    if (node.HasChildNodes)
                        buildTree(treeRoot, node.FirstChild);
                    break;
            }

            if (node.NextSibling != null)
            {
                if (t.Left == null)
                    buildTree(t, node.NextSibling);
                else
                {
                    if (node.ParentNode.ChildNodes.Count <= 3 || node.NextSibling.NextSibling == null)//3 because MathML standard demand <apply><operator/> ..args...</apply> so there is always atleast one childnode
                    {
                        buildTree(t, node.NextSibling);
                    }
                    else
                    {
                        if (t.Right == null)
                        {
                            t.Add(t.Data);
                            buildTree(t.Right, node.NextSibling);
                        }
                        else
                        {
                            throw new Exception("Building tree error");
                            //print(this.tree, 0);
                        }
                    }
                }
            }
        }

        public static byte[] GetBinary(Expression expression)
        {
            Stream s = null;
            byte[] result = null;
            try
            {
                s = new MemoryStream();
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(s, expression);
                result = new byte[s.Length];
                s.Read(result, 0, (int)s.Length);
            }
            finally
            {
                if (s != null)
                    s.Close();
            }

            return result;
        }


        public static bool Save(string filename, Expression expression)
        {
            Stream s = null;
            bool res;
            try
            {
                s = File.Create(filename);
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(s, expression);
                res = true;
            }
            finally
            {
                if (s != null)
                    s.Close();
            }
            return res;
        }


        public static bool Load(string filename, ref Expression expression)
        {
            Stream s = null;
            bool res;
            try
            {
                s = File.OpenRead(filename);
                BinaryFormatter deserializer = new BinaryFormatter();
                expression = (Expression)deserializer.Deserialize(s);
                res = true;
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File doesnt exist");
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e.Message);
                res = false;
            }
            finally
            {
                if (s != null)
                    s.Close();
            }
            return res;
        }
    }
}

