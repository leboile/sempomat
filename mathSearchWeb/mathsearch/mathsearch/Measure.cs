﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mathsearch
{
    public class Imprint
    {
        private List<ImprintColumn> columns = new List<ImprintColumn>();
        private BinaryTreeInfo info;
        public BinaryTreeInfo Info
        {
            get
            {
                return info;
            }
        }
        private Imprint(List<ImprintColumn> cs, BinaryTreeInfo inf)
        {
            columns = cs;
            info = inf;
        }
        public Imprint(BinaryTree root)
        {
            info = root.GetTreeInfo();
            int[] pathToRoot = new int[info.Depth];
            int indexCounter = 0;
            columnGenerator(root, pathToRoot, ref indexCounter, 0);
            columns.Sort();
        }

        private void columnGenerator(BinaryTree node, int[] pathToRoot, ref int indexCounter, int currDepth)
        {
            if (node == null)
                return;
            pathToRoot[currDepth] = indexCounter;
            columns.Add(new ImprintColumn(indexCounter++, node, currDepth, pathToRoot));
            columnGenerator(node.Left, pathToRoot, ref indexCounter, currDepth + 1);
            columnGenerator(node.Right, pathToRoot, ref indexCounter, currDepth + 1);
        }

        private bool JOIN(int depth)
        {
            List<ImprintColumn> transformationList = new List<ImprintColumn>(columns.Where(x => x.Depth == depth));
            List<ImprintColumn> removalList = new List<ImprintColumn>();
            List<ImprintColumn> result = new List<ImprintColumn>();
            bool successfulSum = false;
            for (int i = 0; i < transformationList.Count - 1; i++)
            {
                if (removalList.IndexOf(transformationList[i]) > -1)
                    continue;
                successfulSum = false;
                for (int j = i + 1; j < transformationList.Count; j++)
                    try
                    {
                        if (removalList.IndexOf(transformationList[j]) > -1)
                            continue;
                        ImprintColumn sum = transformationList[i] + transformationList[j];
                        removalList.Add(transformationList[i]);
                        removalList.Add(transformationList[j]);
                        result.Add(sum);
                        successfulSum = true;
                        break;
                    }
                    catch (ArgumentException)
                    {
                        continue;
                    }
                if (!successfulSum)
                    result.Add(transformationList[i]);
            }
            foreach (ImprintColumn c in removalList)
                columns.Remove(c);
            int retValue = 0;
            foreach (ImprintColumn c in result)
            {
                retValue += c.Value;
                columns.Add(c);
            }
            columns.Sort();
            return retValue == 0;
        }

        private void GRAB(int depth, bool grabZeros)
        {
            List<ImprintColumn> transformationList = new List<ImprintColumn>(columns.Where(x => x.Depth == depth));
            List<ImprintColumn> lookupList = new List<ImprintColumn>(columns.Where(x => x.Depth == depth + 1));
            List<ImprintColumn> removalList = new List<ImprintColumn>();
            foreach (ImprintColumn c1 in transformationList)
            {
                if (!grabZeros && c1.Value == 0)
                    continue;
                foreach (ImprintColumn c2 in lookupList)
                {
                    //
                    if ((c1.Value > 0 || (grabZeros && c1.Value + c2.Value == 0)) && c2.Parent == c1.Index)
                    {
                        removalList.Add(c2);
                        c1.Value += c2.Value;
                    }
                }
            }

            foreach (ImprintColumn c in removalList)
            {
                lookupList.Remove(c);
                columns.Remove(c);
            }
            columns.Sort();
        }

        public int[] GetMeasureArray()
        {
            bool joinRes = JOIN(info.Depth - 1);
            //Console.WriteLine("Depth: " + (info.Depth - 1) + "\r\n" + this);
            for (int i = info.Depth - 2; i >= 0; i--)
            {
                GRAB(i, false);//joinRes???
                joinRes = JOIN(i);
                //Console.WriteLine("Depth: " + i + "\r\n" + this);
            }
            //CLEANINGUP
            List<int> measure = new List<int>();
            measure.Add(columns[0].Value);
            for (int i = 1; i < columns.Count; i++)
            {
                if (columns[i].Depth == columns[i - 1].Depth)
                    measure[measure.Count - 1] += columns[i].Value;
                else
                    measure.Add(columns[i].Value);
            }
            return measure.ToArray();
        }

        public Imprint Copy()
        {
            List<ImprintColumn> straightColumns = new List<ImprintColumn>();
            foreach (ImprintColumn i in columns)
                straightColumns.Add(i.Copy());
            BinaryTreeInfo inf = new BinaryTreeInfo();
            inf.Depth = info.Depth;
            inf.SkeletonCount = info.SkeletonCount;
            inf.WFCount = info.WFCount;
            return new Imprint(straightColumns, inf);
        }
        public Imprint InvertedCopy()
        {
            List<ImprintColumn> invertedColumns = new List<ImprintColumn>();
            foreach (ImprintColumn i in columns)
                invertedColumns.Add(i.InvertedCopy());
            BinaryTreeInfo invertedInfo = new BinaryTreeInfo();
            invertedInfo.Depth = info.Depth;
            invertedInfo.SkeletonCount = info.WFCount;
            invertedInfo.WFCount = info.SkeletonCount;
            return new Imprint(invertedColumns, invertedInfo);
        }
        public override string ToString()
        {
            StringBuilder SB = new StringBuilder();
            SB.AppendLine("Total columns: " + columns.Count);
            foreach (ImprintColumn column in columns)
                SB.AppendLine(column.ToString());
            return SB.ToString();
        }
    }

    public class ImprintColumn : IComparable
    {
        private int index;
        public int Index
        {
            get
            {
                return index;
            }
        }
        private int[] pathToRoot;
        public int Parent
        {
            get
            {
                return pathToRoot[0];
            }
        }
        private int val;
        public int Value
        {
            get
            {
                return val;
            }
            set
            {
                val = value;
            }
        }
        private int depth;
        public int Depth
        {
            get
            {
                return depth;
            }
        }
        private ImprintColumn(int ind, int[] path, int d, int v)
        {
            index = ind;
            depth = d;
            pathToRoot = new int[depth];
            path.CopyTo(pathToRoot, 0);
            val = v;
        }
        public ImprintColumn(int ind, BinaryTree node, int d, int[] path)
        {
            index = ind;
            if (node.Data.direction == MarkupType.Skeleton)
                val = 0;
            else
                val = 1;
            depth = d;
            pathToRoot = new int[depth];
            for (int i = depth - 1; i >= 0; i--)
                pathToRoot[depth - 1 - i] = path[i];
        }
        public ImprintColumn Copy()
        {
            return new ImprintColumn(this.index, this.pathToRoot, this.depth, this.val);
        }
        public ImprintColumn InvertedCopy()
        {
            int newVal = 0;
            if (this.val == 0)
                newVal = 1;
            else
                if (this.val > 1)
                    throw new InvalidOperationException("Column's value cannot be inverted");
            return new ImprintColumn(this.index, this.pathToRoot, this.depth, newVal);
        }
        public override string ToString()
        {
            StringBuilder SB = new StringBuilder();
            SB.Append(index + " " + val);
            for (int i = 0; i < depth; i++)
                SB.Append(pathToRoot[i]);
            return SB.ToString();
        }

        public int CompareTo(object obj)
        {
            ImprintColumn column = obj as ImprintColumn;
            if (obj == null)
                return 0;
            if (depth > column.depth)
                return -1;
            else
                if (depth < column.depth)
                    return 1;
                else
                    return 0;
        }

        public static ImprintColumn operator +(ImprintColumn x, ImprintColumn y)
        {
            if (x == null || y == null)
                throw new ArgumentNullException();
            bool validArgs = true;
            if (x.Depth != y.Depth || x.index == -1 || y.index == -1)
                validArgs = false;
            else
                for (int i = 0; i < x.Depth; i++)
                    if (x.pathToRoot[i] != y.pathToRoot[i])
                    {
                        validArgs = false;
                        break;
                    }
            if (!validArgs)
                throw new ArgumentException("Trying to sum " + x.ToString() + " and " + y.ToString());
            return new ImprintColumn(-1, x.pathToRoot, x.Depth, x.val + y.val);
        }
    }

    public class Measure
    {
        //private Imprint imprint;
        private int[] m;
        private int count;
        public int Count
        {
            get
            {
                return count;
            }
        }
        private bool zero;
        public bool Zero
        {
            get
            {
                return zero;
            }
        }
        private bool inverted;
        public bool Inverted
        {
            get
            {
                return inverted;
            }
        }

        private int buildMeasureArray(BinaryTree tree, List<int[]> measureArray, int currentDepth)
        {
            if (tree == null)
                return 0;
            int WFSize = 0;
            int depth = currentDepth;
            if ((tree.Data.direction != MarkupType.Skeleton && Inverted == false) || (tree.Data.direction == MarkupType.Skeleton && Inverted == true))
            {
                WFSize++;
                currentDepth--;
            }
            WFSize += buildMeasureArray(tree.Left, measureArray, currentDepth + 1) + buildMeasureArray(tree.Right, measureArray, currentDepth + 1);

            if (tree.Parent != null && ((tree.Parent.Data.direction != MarkupType.Skeleton && Inverted == false) || (tree.Parent.Data.direction == MarkupType.Skeleton && Inverted == true)))
            {
                return WFSize;
            }
            else
            //if (tree.Data.direction != WaveFrontDirection.Base)
            {
                int[] entry = new int[2];
                entry[0] = WFSize;
                entry[1] = depth;
                measureArray.Add(entry);
                return 0;
            }
        }


        public Measure(BinaryTree tree)
        {
            inverted = false;
            List<int[]> measureArray = new List<int[]>();
            int res = buildMeasureArray(tree, measureArray, 0);
            if (res != 0)
                throw new InvalidOperationException("buildMeasureArray failed");
            List<int> nextArr = new List<int>();
            for (int i = 0; i < tree.GetTreeInfo().Depth; i++)
            {
                int val = 0;
                bool toInsert = false;
                foreach (int[] entry in measureArray)
                    if (entry[1] == i)
                    {
                        val += entry[0];
                        toInsert = true;
                    }
                if (toInsert)
                    nextArr.Add(val);
            }

            count = nextArr.Count;
            if (count == 0)
            {
                zero = true;
                return;
            }
            else
                zero = false;
            m = new int[count];
            for (int i = nextArr.Count - 1; i >= 0; i--)
                m[nextArr.Count - i - 1] = nextArr[i];
        }

        public Measure(BinaryTree tree, bool invert)
        {
            inverted = invert;
            List<int[]> measureArray = new List<int[]>();
            int res = buildMeasureArray(tree, measureArray, 0);
            if (res != 0)
                throw new InvalidOperationException("buildMeasureArray failed");
            List<int> nextArr = new List<int>();
            for (int i = 0; i < tree.GetTreeInfo().Depth; i++)
            {
                int val = 0;
                bool toInsert = false;
                foreach (int[] entry in measureArray)
                    if (entry[1] == i)
                    {
                        val += entry[0];
                        toInsert = true;
                    }
                if (toInsert)
                    nextArr.Add(val);
            }
            count = nextArr.Count;
            if (count == 0)
            {
                zero = true;
                return;
            }
            else
                zero = false;
            m = new int[count];
            for (int i = nextArr.Count - 1; i >= 0; i--)
                m[nextArr.Count - i - 1] = nextArr[i];
        }

        public Measure(Imprint i)
        {
            m = i.Copy().GetMeasureArray();
            count = m.Count();
            if (count == 0)
                zero = true;
            else
                zero = false;
        }

        private Measure(int[] measure)//deprecated
        {
            zero = false;
            count = measure.Count();
            if (count == 0)
                zero = true;
            m = measure;
        }

        public int WavefrontCount
        {
            get
            {
                int sum = 0;
                for (int i = 0; i < count; i++)
                    sum += m[i];
                return sum;
            }
        }

        public bool isEnd
        {
            get
            {
                for (int i = 0; i < count - 1; i++)
                    if (m[i] > 0)
                        return false;
                return true;
            }
        }

        public int LastNum
        {
            get
            {
                return m[m.Count() - 1];
            }
        }

        public Measure Shrunk(int index)
        {
            int[] newM = new int[Count - 1];
            for (int i = 0, j=0; i < Count-1; i++, j++)
            {
                newM[i] = m[j];
                if (i == index - 1)
                {
                    newM[i] = m[j + 1];
                    j++;
                }
            }
            return new Measure(newM);
        }
        //public static Measure Shrunk(BinaryTree enlargementNode, Measure target)
        //{
        //    if (enlargementNode.Parent == null)
        //    {
        //        int[] newM = new int[target.Count - 1];
        //        for (int i = 0; i < target.Count - 1; i++)
        //            newM[i] = target.m[i];
        //        return new Measure(newM);
        //    }
        //    else
        //    {
        //        int WFCount = getWFAroundWH(enlargementNode.Parent);

        //    }
        //}
        //private static int getWFAroundWH(BinaryTree node)
        //{
        //    int res=0;
        //    if (node!= null && node.Data.direction != WaveFrontDirection.Base)
        //        res = 1;
        //    else
        //        return 0;
        //    res += getWFAroundWH(node.Parent) + getWFAroundWH(node.Left) + getWFAroundWH(node.Right);
        //    return res;
        //}

        public static Measure operator +(Measure m1, Measure m2)
        {
            if (m1.zero)
                return m2;
            if (m2.zero)
                return m1;
            if (m1.count != m2.count)
                throw new InvalidOperationException("Unsummable measures " + m1.ToString() + " and " + m2.ToString());//or return null??
            int[] newMeasure = new int[m1.count];
            for (int i = 0; i < m1.count; i++)
                newMeasure[i] = m1.m[i] + m2.m[i];
            return new Measure(newMeasure);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            Measure m = obj as Measure;
            if (m == null)
                return false;
            return this == m;
        }

        public static bool operator ==(Measure m1, Measure m2)
        {
            if ((object)m1 == null && (object)m2 == null)
                return true;
            if ((object)m1 == null || (object)m2 == null || m1.count != m2.count)
                return false;
            else
                for (int i = 0; i < m1.count; i++)
                    if (m1.m[i] != m2.m[i])
                        return false;
            return true;
        }
        public static bool operator !=(Measure m1, Measure m2)
        {
            if ((object)m1 == null || (object)m2 == null || m1.count != m2.count)
                return false;
            else
                return !(m1 == m2);
        }
        public static bool operator >(Measure m1, Measure m2)
        {
            if (m1 == null || m2 == null || m1.count != m2.count)
            {
                if (m1.zero)
                    return false;
                else
                    if (m2.zero)
                        return true;
                throw new InvalidOperationException("Uncomparable measures " + m1 + " and " + m2);
            }
            for (int i = 0; i < m1.count; i++)
                if (m1.m[i] > m2.m[i])
                    return true;
                else
                    if (m1.m[i] < m2.m[i])
                        return false;
            return false;
        }

        public static bool operator <(Measure m1, Measure m2)
        {
            if (m1 == null || m2 == null || m1.count != m2.count)
            {
                if (m2.zero)
                    return false;
                else
                    if (m1.zero)
                        return true;
                throw new InvalidOperationException("Uncomparable measures " + m1.ToString() + " and " + m2.ToString());
            }
            for (int i = 0; i < m1.count; i++)
                if (m1.m[i] < m2.m[i])
                    return true;
                else
                    if (m1.m[i] > m2.m[i])
                        return false;
            return false;
        }

        public override string ToString()
        {
            if (zero)
                return "Zero";
            string res = "";
            for (int i = 0; i < count; i++)
                res += m[i] + " ";
            return res;
        }
    }
}
