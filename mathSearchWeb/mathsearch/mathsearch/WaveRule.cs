﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
namespace mathsearch
{

    public class WaveRuleApplyHelper
    {
        private WaveRule WR;
        private Dictionary<Element, BinaryTree> substTable; //key - elements from rule; value - elements from original tree
        public WaveRuleApplyHelper(WaveRule rule)
        {
            WR = rule;
            substTable = new Dictionary<Element, BinaryTree>();
        }
        private bool checkStructure(BinaryTree tree, BinaryTree leftRule)
        {
            if (leftRule == null && tree == null)
                return true;
            if (leftRule == null || tree == null)
                return false;
            switch (leftRule.Data.type)
            {
                case ElementType.Function:
                case ElementType.ServiceObject://MATH, but can be not only math in future so be aware
                    if (leftRule.Data != tree.Data || leftRule.Data.direction != tree.Data.direction)
                        return false;
                    //Dictionary<Element, BinaryTree> backup = new Dictionary<Element, BinaryTree>(substTable);
                    if (checkStructure(tree.Left, leftRule.Left) && checkStructure(tree.Right, leftRule.Right))
                        return true;
                    else
                    {
                        //substTable = new Dictionary<Element, BinaryTree>(backup);
                        //if (checkStructure(tree.Left, leftRule.Right) && checkStructure(tree.Right, leftRule.Left))//DANGEROUS
                        //    return true;
                        //else
                        return false;
                    }
                case ElementType.Variable:
                    if (tree.Data.type == ElementType.ServiceObject || !checkDirection(tree, leftRule.Data.direction) || leftRule.Data == tree.Data)
                        return false;
                    try
                    {
                        substTable.Add(leftRule.Data, tree);
                        return true;
                    }
                    catch (ArgumentException)
                    {
                        BinaryTree t;
                        if (substTable.TryGetValue(leftRule.Data, out t))
                        {
                            if (t == tree && tree!=null)
                                return true;
                            else
                                return false;
                        }
                        else
                            throw new Exception("Something went totally wrong");
                    }
                case ElementType.Constant:
                    if (leftRule.Data != tree.Data || leftRule.Data.direction != tree.Data.direction)
                        return false;
                    else
                        return true;
                default:
                    return false;
            }
        }
        private bool checkDirection(BinaryTree tree, MarkupType d)
        {
            if (tree == null)
                return true;
            if (tree.Data.direction != d)
                return false;
            else
            {
                return checkDirection(tree.Left, d) && checkDirection(tree.Right, d);
            }
        }

        public bool isAppliable(BinaryTree tree)
        {
            substTable.Clear();
            bool answer = checkStructure(tree, WR.leftPart.DeepCopy());
            if (answer)
            {
                //Console.WriteLine("SUBSTTABLE: ");
                //foreach (KeyValuePair<Element, BinaryTree> wre in substTable)
                //{
                //    Console.WriteLine(wre.Key.name + " ---- " + wre.Value.Data.name);
                //}
                //Console.WriteLine("END SUBSTTABLE");
            }
            return answer;
        }

        public bool applyRule(BinaryTree tree)
        {
            if (isAppliable(tree))
            {
                BinaryTree substTree = WR.rightPart.DeepCopy();
                //Console.WriteLine("right part: ");
                //rightPart.Print();
                //Console.WriteLine("right part end");
                //foreach (KeyValuePair<Element, BinaryTree> pair in substTable)
                //{
                //    Console.WriteLine(pair.Key);
                //    Console.WriteLine("ON");
                //    pair.Value.Print();
                //    Console.WriteLine();
                //}
                //Console.WriteLine("Stack would be overflowed...");
                WaveRule.Substitute(substTree, substTable);//now i reject your reality and substitute my own
                //Console.WriteLine("NOT");
                //Console.WriteLine("Before: ");
                //tree.Print();
                //Console.WriteLine("After: ");
                //substTree.Print();
                tree.Left = substTree.Left;
                tree.Right = substTree.Right;
                tree.Data = substTree.Data;
                //Console.WriteLine(description + " rule applied!");
                //Console.ReadLine();
                return true;
            }
            else
                return false;
        }
    }

    [Serializable()]
    public class WaveRule
    {
        public BinaryTree leftPart;
        public BinaryTree rightPart;
        public string description;
        private Dictionary<Element, BinaryTree> substTable; //key - elements from rule; value - elements from original tree
        public WaveRule(BinaryTree l, BinaryTree r, string descr)
        {
            description = descr;
            leftPart = l;
            rightPart = r;
            substTable = new Dictionary<Element, BinaryTree>();
        }

        public static bool Save(string filename, WaveRule rule)
        {
            Stream s = null;
            bool res;
            try
            {
                s = File.Create(filename);
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(s, rule);
                res = true;
            }
            finally
            {
                if (s != null)
                    s.Close();
            }
            return res;
        }
        public static bool Load(string filename, ref WaveRule rule)
        {
            Stream s = null;
            bool res;
            try
            {
                s = File.OpenRead(filename);
                BinaryFormatter deserializer = new BinaryFormatter();
                rule = (WaveRule)deserializer.Deserialize(s);
                res = true;
            }
            finally
            {
                if (s != null)
                    s.Close();
            }
            return res;
        }

        public static void Substitute(BinaryTree tree, Dictionary<Element, BinaryTree> substTable)//so much room for optimization
        {
            if (tree != null)
            {
                Substitute(tree.Left, substTable);
                Substitute(tree.Right, substTable);
            }
            else
                return;

            bool substAgain = false;
            BinaryTree value;
            if (tree.Left != null)
            {
                substTable.TryGetValue(tree.Left.Data, out value);
                if (value != null)
                {
                    MarkupType dir = tree.Left.Data.direction;
                    tree.Left = value.DeepCopy();
                    //Substitute(tree.Left, substTable);
                    substAgain = true;
                    Markup.SetMarkup(tree.Left, dir);
                }
                //else
                //    Console.WriteLine("Left NULL "+tree.Left.Data);
            }
            value = null;
            if (tree.Right != null)
            {
                substTable.TryGetValue(tree.Right.Data, out value);
                if (value != null)
                {
                    MarkupType dir = tree.Right.Data.direction;
                    tree.Right = value.DeepCopy();
                    //Substitute(tree.Right, substTable);
                    substAgain = true;
                    Markup.SetMarkup(tree.Right, dir);
                }
                //else
                //    Console.WriteLine("Right NULL "+tree.Right.Data);
            }
            if (substAgain)
                Substitute(tree, substTable);
        }

        public static void CreateWaveRules(string first, string second, string name)
        {
            XmlDocument x1 = new XmlDocument();
            XmlDocument x2 = new XmlDocument();
            x1.Load(first);
            x2.Load(second);
            Expression e1 = new Expression(x1.DocumentElement, true);
            Expression e2 = new Expression(x2.DocumentElement, true);

            Console.WriteLine("Expression #1");
            Console.WriteLine(e1);
            Console.WriteLine("And expression #2");
            Console.WriteLine(e2);

            List<UnificationEntry> unif = Markup.Annotate(e1, e2, false, MinimalityOption.NoRestrictions);

            int rulesCount = 0;
            int minimalCount = 0;
            int notPreserving = 0;
            foreach (UnificationEntry e in unif)
            {
                rulesCount++;
               
                Console.WriteLine("First: ");
                Console.WriteLine(e.GivenRoot);
                Console.WriteLine();
                Console.WriteLine("Second: ");
                 Console.WriteLine(e.GoalRoot);


                if (Rippling.isSkeletonPreserving(e.GivenRoot, e.GoalRoot))
                {
                    Console.WriteLine("SKELETON PRESERVING");
                    //Console.WriteLine(t[0].Includes(t[1].GetSkeleton()));
                }
                else
                {

                    Console.WriteLine("SKELETON NOT PRESERVING");
                    Console.ReadLine();
                    notPreserving++;
                    continue;
                }

                Measure m1 = new Measure(e.GivenRoot);
                Measure m2 = new Measure(e.GoalRoot);
                Console.WriteLine("M1: " + m1 + " M2: " + m2);
                if (!(m1 > m2))
                {
                    Console.WriteLine("NOT measure decreasing");
                    continue;
                }
                else
                {
                    minimalCount++;
                }

                WaveRule WR = new WaveRule(e.GivenRoot, e.GoalRoot, name+"#"+rulesCount);
                WaveRule.Save("rules/" + name + rulesCount, WR);

                Console.WriteLine("Saved");
                Console.WriteLine("---------------------END OF " +rulesCount + " -------------------");
                //Console.ReadLine();
            }
            Console.WriteLine("TOTAL:" + unif.Count + " Uniq: " + rulesCount + " Skeleton not preserving: " + notPreserving + " minimal: " + minimalCount);
        }
    }
}
