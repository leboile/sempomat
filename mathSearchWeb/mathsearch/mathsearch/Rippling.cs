﻿using System;
using System.Collections.Generic;
using System.IO;
///TODO:
///Реализовать коммутирование быстрее, если это возможно. 
///не забывать про clipping of maximal и динамическое определение количества потоков
///Волновые правила неполны их надо все задавать вручную или же написать корректную реализацию DUNIFY.
///по сути rippling step вообще бесполезен. Хоть и выбирается лучший результат полноценного док-ва не происходит и результат
///если и улучшается, то незначительно
///SHRUNK!!!!
///Баги, ошибки, неточности.
namespace mathsearch
{
    public enum MeasureComparisonMode
    {
        Invert,
        Shrunk
    }
    public static class Rippling
    {
        public static Log log = new Log("C:\\Users\\adm\\Desktop\\mathSearchWeb\\RuleCreator\\bin\\Debug");
        private static List<WaveRule> rules;
        private static ThreadMagazine magazine = null;
        internal static ThreadMagazine Magazine
        {
            get
            {
                return magazine;
            }
        }
        private static double limit;
        private static MeasureComparisonMode measureComparisonMode;
        public static double Limit
        {
            get
            {
                return limit;
            }
        }

        public static int LoadRules(string rulesDir)
        {
            rules = new List<WaveRule>();
            int count = 0;
            foreach (string path in Directory.GetFiles(rulesDir))
            {
                WaveRule WR = new WaveRule(null, null, null);
                WaveRule.Load(path, ref WR);
                if (WR.leftPart == null || WR.rightPart == null)
                    throw new FileLoadException(path);
                rules.Add(WR);
                log.Add(WR.description);
                log.Add(WR.leftPart.PlainText() + "-->" + WR.rightPart.PlainText());
                count++;
            }
            //Console.WriteLine(count + " rules loaded");
            return count;
        }

        public static BinaryTree GetLastSubst(BinaryTree t, Dictionary<Element, BinaryTree> substTable)
        {
            BinaryTree value;
            if (t != null)
            {
                if (substTable.TryGetValue(t.Data, out value))
                {
                    if (value != null)
                        return GetLastSubst(value, substTable);
                }
            }
            return t;
        }

        public static bool isSkeletonPreserving(BinaryTree t1, BinaryTree t2)
        {
            Skeleton sk1;
            //if (t1.Expr == null)
            sk1 = new Skeleton(t1);
            //else
            //    sk1 = t1.Expr.Skeleton;

            Skeleton sk2;
            //if (t2.Expr == null)
            sk2 = new Skeleton(t2);
            //else
            //    sk2 = t2.Expr.Skeleton;
            return sk1 == sk2;
        }

        public static double Eq(Expression e1, Expression e2, double precision, bool ForceStopAfterAnnotation, bool ClippingOfMaximal, MeasureComparisonMode Mode)
        {
            measureComparisonMode = Mode;
            limit = precision;
            magazine = new ThreadMagazine(5, ClippingOfMaximal);
            Markup.Annotate(e1, e2, true, MinimalityOption.CostAndSubstMinimal);//starting the conveyor
            log.Add("Annotation finished");
            magazine.Finish(!ForceStopAfterAnnotation);
            log.FlushOnScreen();
            return ThreadMagazine.maxWeight;
        }

        public struct SkeletonEnlargementOption
        {
            public BinaryTree givenNode;
            public BinaryTree goalNode;
            public Measure givenMeasure;
            public Measure goalMeasure;
            public SkeletonEnlargementOption(BinaryTree givenN, BinaryTree goalN, Measure givenM, Measure goalM)
            {
                givenNode = givenN;
                goalNode = goalN;
                givenMeasure = givenM;
                goalMeasure = goalM;
            }
        }

        public static List<SkeletonEnlargementOption> GetSkeletonEnlargements(BinaryTree given, BinaryTree goal)
        {
            List<SkeletonEnlargementOption> result = new List<SkeletonEnlargementOption>();
            result.Add(new SkeletonEnlargementOption(null, null, null, null));
            BinaryTree start = given.First;
            while (start != null)
            {
                if (start.Data.direction != MarkupType.Skeleton)//if node is candidate for enlargement
                {
                    MarkupType givendir = start.Data.direction;
                    start.Data.direction = MarkupType.Skeleton;//Enlarging given skeleton
                    List<BinaryTree> goalCandidates = new List<BinaryTree>();
                    goal.FindAll(start.Data, ref goalCandidates);
                    foreach (BinaryTree node in goalCandidates)
                    {
                        if (node.Data.direction != MarkupType.Skeleton)
                        {
                            MarkupType goaldir = node.Data.direction;
                            node.Data.direction = MarkupType.Skeleton;//enlargin goal skeleton
                            if (isSkeletonPreserving(given, goal))//checking if we've got correct skeleton enlargement
                            {
                                result.Add(new SkeletonEnlargementOption(start, node, new Measure(given), new Measure(goal)));
                            }
                            node.Data.direction = goaldir;
                        }
                    }
                    start.Data.direction = givendir;
                }
                start = start.Next;
            }

            return result;
        }

        public static bool buildRelationTable(BinaryTree given, BinaryTree goal, ref Dictionary<Variable, HashSet<Variable>> relTable, bool commutativeParent)//also determining equivalence of trees till vars
        {
            if (given == null && goal == null)
                return true;
            if (given == null || goal == null)
                return false;
            if (given.Data.type == ElementType.Variable && goal.Data.type == ElementType.Variable)
            {
                HashSet<Variable> lockedVars = null;
                if (!relTable.TryGetValue(new Variable("####LOCKEDVARS####"), out lockedVars))
                {
                    lockedVars = new HashSet<Variable>();
                    relTable.Add(new Variable("####LOCKEDVARS####"), lockedVars);
                }

                if (commutativeParent && goal.Data != given.Data)//without goal!=given b+c and d+c produce c: c b d entry in relTable
                {
                    if (!lockedVars.Contains((Variable)given.Data) && !lockedVars.Contains((Variable)goal.Data))
                    {
                        if (relTable.ContainsKey((Variable)given.Data))
                            relTable[(Variable)given.Data].Add((Variable)goal.Data);
                        else
                        {
                            HashSet<Variable> value = new HashSet<Variable>();
                            value.Add((Variable)goal.Data);
                            relTable.Add((Variable)given.Data, value);
                        }

                        if (relTable.ContainsKey((Variable)goal.Data))
                            relTable[(Variable)goal.Data].Add((Variable)given.Data);
                        else
                        {
                            HashSet<Variable> value = new HashSet<Variable>();
                            value.Add((Variable)given.Data);
                            relTable.Add((Variable)goal.Data, value);
                        }
                    }
                }
                else
                {
                    if (relTable.ContainsKey((Variable)given.Data))
                        relTable.Remove((Variable)given.Data);
                    HashSet<Variable> value1 = new HashSet<Variable>();
                    value1.Add((Variable)goal.Data);
                    relTable.Add((Variable)given.Data, value1);

                    if (goal.Data != given.Data)
                    {
                        if (relTable.ContainsKey((Variable)goal.Data))
                            relTable.Remove((Variable)goal.Data);
                        HashSet<Variable> value2 = new HashSet<Variable>();
                        value2.Add((Variable)given.Data);
                        relTable.Add((Variable)goal.Data, value2);
                    }

                    //locking those variable, because relation between them is determined
                    lockedVars.Add((Variable)given.Data);
                    lockedVars.Add((Variable)goal.Data);
                }
                return true;
            }
            else
                if (given.Data == goal.Data)
                {
                    if (given.Data.type == ElementType.Function && ((Function)given.Data).Commutative)//means that goal.Datat.type == Function
                    {
                        //but what if both states of commutative function suits?
                        Dictionary<Variable, HashSet<Variable>> relTableCopy = new Dictionary<Variable, HashSet<Variable>>(relTable);

                        //commutativity only matters if both options would make given and goal equal
                        bool Commutative = commutativeParent; ;
                        int givenChildren = 0, goalChildren = 0;

                        if (given.Left != null)
                            givenChildren++;
                        if (given.Right != null)
                            givenChildren++;

                        if (goal.Left != null)
                            goalChildren++;
                        if (goal.Right != null)
                            goalChildren++;

                        if (givenChildren != goalChildren)
                            return false;
                        else
                            switch (givenChildren)
                            {
                                case 0://leaf node so commutativity doesnt matter
                                    return true;
                                case 1://commutativity doesnt matter
                                    Commutative = commutativeParent;
                                    break;
                                case 2:
                                    //below the condition when commutativity of function matters
                                    if ((given.Left.Data == goal.Left.Data && given.Right.Data == goal.Right.Data
                                        && given.Right.Data == goal.Left.Data && given.Left.Data == goal.Right.Data)
                                        || (given.Left.Data.type == ElementType.Variable && given.Right.Data.type == ElementType.Variable
                                            && goal.Left.Data.type == ElementType.Variable && goal.Right.Data.type == ElementType.Variable))
                                        Commutative = true;
                                    break;
                            }

                        if (buildRelationTable(given.Left, goal.Left, ref relTableCopy, Commutative) && buildRelationTable(given.Right, goal.Right, ref relTableCopy, Commutative))
                        {
                            relTable = new Dictionary<Variable, HashSet<Variable>>(relTableCopy);//one state of args of commutative function is suitable, so its time to restore backup
                            //trying another state with Copy, if function returns true then relTable will be changed with its "copy" again
                            if (buildRelationTable(given.Right, goal.Left, ref relTableCopy, Commutative) && buildRelationTable(given.Left, goal.Right, ref relTableCopy, Commutative))
                                relTable = new Dictionary<Variable, HashSet<Variable>>(relTableCopy);
                            return true;
                        }
                        else
                            return buildRelationTable(given.Right, goal.Left, ref relTable, commutativeParent) && buildRelationTable(given.Left, goal.Right, ref relTable, commutativeParent);
                    }
                    else
                        return buildRelationTable(given.Left, goal.Left, ref relTable, commutativeParent) && buildRelationTable(given.Right, goal.Right, ref relTable, commutativeParent);
                }
                else
                    return false;
        }

        private static int DFSAnalysis(Dictionary<Variable, HashSet<Variable>> relTable, Variable v, bool DegEqualsOne, ref int VarsDegMoreThanOne)
        {
            Variable[] neighbours = new Variable[relTable[v].Count];
            relTable[v].CopyTo(neighbours);
            if (neighbours.Length == 0)//already visited
                return 0;
            else
                relTable[v].Clear();//paying a visit
            int res = 1;
            bool DegOne = false;
            if (neighbours.Length == 1)
                DegOne = true;
            else
                if (neighbours.Length > 1)
                    VarsDegMoreThanOne++;
                else
                    throw new Exception("No neighbours");
            int degOneCount = 0;
            if (DegEqualsOne)
                degOneCount++;
            foreach (Variable var in neighbours)
            {
                if (relTable[var] != null && relTable[var].Count == 1)
                    degOneCount++;
                res += DFSAnalysis(relTable, var, DegOne, ref VarsDegMoreThanOne);
            }
            if (degOneCount > 1)
                return Int32.MinValue;
            else
                return res;
        }

        public static bool checkEqAndCorelateVars(BinaryTree given, BinaryTree goal)//checking tree eq and trying to get variables relation
        {
            Dictionary<Variable, HashSet<Variable>> relTable = new Dictionary<Variable, HashSet<Variable>>();
            bool result = Rippling.buildRelationTable(given, goal, ref relTable, false);
            if (!result)
                return false;
            //reltable analysis
            foreach (KeyValuePair<Variable, HashSet<Variable>> pair in relTable)
            {
                Variable v = pair.Key;
                if (v.name == "####LOCKEDVARS####")//magic name
                    continue;
                int VarsDegMoreThanOne = 0;
                int connectedComponent = DFSAnalysis(relTable, v, false, ref VarsDegMoreThanOne);
                //Console.WriteLine("Connected component: " + connectedComponent+" "+VarsDegMoreThanOne);
                if (connectedComponent < 0 || (connectedComponent > 2 && (connectedComponent % 2 == 1 || VarsDegMoreThanOne < 2)))
                    return false;
            }

            return true;
        }

        internal static void ripplingStep(BinaryTree given, BinaryTree goal, BinaryTree oldGiven, BinaryTree oldGoal, Measure m, ref RipplingStepResult result)
        {
            Measure mGoal = new Measure(goal);
            Measure mGiven = new Measure(given);

            if (m.isEnd)
            {
                if (checkEqAndCorelateVars(given, goal))
                    result.AddResult(given, goal, true);
                else
                    result.AddResult(given, goal, false);
                return;
            }

            if (isSkeletonPreserving(given, goal))
            {
                //Console.WriteLine("Skeleton preserving");
                result.AddResult(given, goal, false);
            }
            else
            {
                log.Add("NOT Skeleton preserving");
                log.Add(given.ToString());
                log.Add(goal.ToString());
                return;
            }

            foreach (WaveRule WR in rules)
            {
                BinaryTree newGiven = given.DeepCopy();
                BinaryTree newGoal = goal.DeepCopy();
                if (searchToApplyRule(newGiven, WR))
                {
                    result.NumberOfRulesApplied++;
                    ripplingStep2(newGiven, newGoal, given, goal, m, ref result);
                    if (result.IsFinished)
                        return;
                    result.NumberOfRulesApplied--;
                }

                newGiven = given.DeepCopy();
                newGoal = goal.DeepCopy();
                if (searchToApplyRule(newGoal, WR))
                {
                    result.NumberOfRulesApplied++;
                    ripplingStep2(newGiven, newGoal, given, goal, m, ref result);
                    if (result.IsFinished)
                        return;
                    result.NumberOfRulesApplied--;
                }
            }
            return;
        }

        private static void ripplingStep2(BinaryTree newGiven, BinaryTree newGoal, BinaryTree given, BinaryTree goal, Measure m, ref RipplingStepResult result)
        {
            if (!isSkeletonPreserving(newGiven, newGoal))
            {
                log.Add("NOT Skeleton preserving");
                log.Add(given.ToString());
                log.Add("-");
                log.Add(goal.ToString());
                log.Add("-");
                log.Add(newGiven.ToString());
                log.Add("-");
                log.Add(newGoal.ToString());
                throw new InvalidOperationException("Not skeleton preserving");
            }

            Measure oldMeasure = new Measure(newGiven) + new Measure(newGoal);

            List<SkeletonEnlargementOption> enlargements = GetSkeletonEnlargements(newGiven, newGoal);
            foreach (SkeletonEnlargementOption option in enlargements)
            {
                BinaryTree newGivenVar = null;
                BinaryTree newGoalVar = null;
                Measure newMeasure;
                if (option.goalMeasure == null && option.givenMeasure == null)
                    newMeasure = null;
                else
                    newMeasure = option.givenMeasure + option.goalMeasure;

                if (newMeasure == null)
                {
                    newMeasure = new Measure(newGoal) + new Measure(newGiven);
                    newGivenVar = newGiven.DeepCopy();
                    newGoalVar = newGoal.DeepCopy();
                }

                if (measureComparisonMode == MeasureComparisonMode.Shrunk)
                {
                    Measure comparisonMeasure;
                    if (oldMeasure.Count == newMeasure.Count)
                        comparisonMeasure = oldMeasure;
                    else
                    {
                        MarkupType dirGiven = option.givenNode.Data.direction;
                        MarkupType dirGoal = option.goalNode.Data.direction;
                        option.givenNode.Data.direction = MarkupType.Skeleton;
                        option.goalNode.Data.direction = MarkupType.Skeleton;

                        comparisonMeasure = option.givenMeasure.Shrunk(Skeleton.GetDepthOfNodeInSkeleton(option.givenNode)) + option.goalMeasure.Shrunk(Skeleton.GetDepthOfNodeInSkeleton(option.goalNode));

                        newGivenVar = newGiven.DeepCopy();
                        newGoalVar = newGoal.DeepCopy();
                        option.givenNode.Data.direction = dirGiven;
                        option.goalNode.Data.direction = dirGoal;
                    }

                    if (newMeasure != null && comparisonMeasure < m)
                    {
                        ripplingStep(newGivenVar, newGoalVar, given.DeepCopy(), goal.DeepCopy(), newMeasure, ref result);
                        if (result.IsFinished)
                            return;
                    }
                    else
                        if (newMeasure != null)
                            result.AddResult(newGivenVar, newGoalVar, false);
                }
                else
                {
                    Measure invertedGoalMeasure = new Measure(newGoal, true);
                    Measure invertedGivenMeasure = new Measure(newGiven, true);
                    try
                    {
                        if (invertedGivenMeasure + invertedGoalMeasure < m)
                            ripplingStep(newGiven.DeepCopy(), newGoal.DeepCopy(), given.DeepCopy(), goal.DeepCopy(), newMeasure, ref result);
                    }
                    catch (InvalidOperationException)//performing SHRUNK
                    {
                        log.Add("InvertedGoalM: " + invertedGoalMeasure + " InvertedGoalM: " + invertedGivenMeasure + " newMeasure: " + newMeasure);
                        Measure comparisonMeasure;
                        if (oldMeasure.Count == newMeasure.Count)
                            comparisonMeasure = oldMeasure;
                        else
                        {
                            MarkupType dirGiven = option.givenNode.Data.direction;
                            MarkupType dirGoal = option.goalNode.Data.direction;
                            option.givenNode.Data.direction = MarkupType.Skeleton;
                            option.goalNode.Data.direction = MarkupType.Skeleton;

                            comparisonMeasure = option.givenMeasure.Shrunk(Skeleton.GetDepthOfNodeInSkeleton(option.givenNode)) + option.goalMeasure.Shrunk(Skeleton.GetDepthOfNodeInSkeleton(option.goalNode));

                            newGivenVar = newGiven.DeepCopy();
                            newGoalVar = newGoal.DeepCopy();
                            option.givenNode.Data.direction = dirGiven;
                            option.goalNode.Data.direction = dirGoal;
                        }

                        if (newMeasure != null && comparisonMeasure < m)
                        {
                            ripplingStep(newGivenVar, newGoalVar, given.DeepCopy(), goal.DeepCopy(), newMeasure, ref result);
                            if (result.IsFinished)
                                return;
                        }
                        else
                            if (newMeasure != null)
                                result.AddResult(newGivenVar, newGoalVar, false);
                    }
                }
            }
            return;
        }

        private static bool searchToApplyRule(BinaryTree target, WaveRule rule)
        {
            //Console.WriteLine("trying to apply "+rule.description);
            List<BinaryTree> applyCandidates = new List<BinaryTree>();
            target.FindAll(rule.leftPart.Data, ref applyCandidates);
            WaveRuleApplyHelper applier = new WaveRuleApplyHelper(rule);
            foreach (BinaryTree applyNode in applyCandidates)
            {
                if (applier.applyRule(applyNode))
                {
                    return true;
                }
            }
            return false;
        }
    }
}

