﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace mathsearch
{
    class Program
    {
        static void Main(string[] args)
        {
            //oldgoal
            //BinaryTree t1 = new BinaryTree(new Function("p", WaveFrontDirection.Base));
            //t1.Add(new Variable("a", WaveFrontDirection.Base));
            //t1.Add(new Function("p",WaveFrontDirection.Base));
            //t1.Right.Add(new Function("f", WaveFrontDirection.Base));
            //t1.Right.Left.Add(new Variable("b", WaveFrontDirection.WaveFrontUp));
            //t1.Right.Left.Add(new Variable("c", WaveFrontDirection.WaveFrontUp));
            //t1.Right.Add(new Variable("e", WaveFrontDirection.Base));

            //BinaryTree t2 = new BinaryTree(new Function("p", WaveFrontDirection.Base));
            //t2.Add(new Variable("a", WaveFrontDirection.Base));
            //t2.Add(new Function("p", WaveFrontDirection.Base));
            //t2.Right.Add(new Function("f", WaveFrontDirection.Base));
            //t2.Right.Left.Add(new Variable("a", WaveFrontDirection.WaveFrontUp));
            //t2.Right.Left.Add(new Variable("d", WaveFrontDirection.WaveFrontUp));
            //t2.Right.Add(new Variable("e", WaveFrontDirection.Base));

            //t2.Print();
            //Console.WriteLine("AND");
            //t1.Print();
            //Console.WriteLine("-----");
            //t2.PlainPrint();
            //t1.PlainPrint();
            //Console.WriteLine("Start: ");
            //Console.WriteLine("Skeleton preserving: " + Rippling.isSkeletonPreserving(t2, t1) +" measure: "+(new Measure(t1)+new Measure(t2)));
            //Console.ReadLine();
            

            //SKELETON ENLARGEMENT TEST
            //BinaryTree t1 = new BinaryTree(new Function("p", WaveFrontDirection.WaveFrontUp));
            //t1.Add(new Variable("a", WaveFrontDirection.WaveFrontUp));
            //t1.Add(new Function("p", WaveFrontDirection.Base));
            //t1.Right.Add(new Function("f", WaveFrontDirection.Base));
            //t1.Right.Left.Add(new Variable("b", WaveFrontDirection.WaveFrontUp));
            //t1.Right.Left.Add(new Variable("c", WaveFrontDirection.WaveFrontUp));
            //t1.Right.Add(new Variable("e", WaveFrontDirection.Base));

            //BinaryTree t2 = t1.DeepCopy();
            //List<Rippling.SkeletonEnlargementOption>result =Rippling.GetSkeletonEnlargements(t1, t2);
            //foreach (Rippling.SkeletonEnlargementOption option in result)
            //{
            //    Console.WriteLine("**********OPTION**************");
            //    if (option.m == null)
            //        continue;
            //    option.givenNode.Data.direction = WaveFrontDirection.Base;
            //    option.goalNode.Data.direction = WaveFrontDirection.Base;
            //    t1.PlainPrint();
            //    t2.PlainPrint();
            //    option.givenNode.Data.direction = WaveFrontDirection.WaveFrontUp;
            //    option.goalNode.Data.direction = WaveFrontDirection.WaveFrontUp;
            //}
            //Console.ReadLine();
            //return;

            //MEASURE TEST
            //BinaryTree t = new BinaryTree(new Function("plus", WaveFrontDirection.WaveFrontUp));
            //t.Add(new Variable("b"));
            //t.Add(new Variable("a"));
            //Console.WriteLine("Measure: " + new Measure(t));
            //Console.ReadLine();
            //return;

            //Console.SetBufferSize(1024, 4096);
            //Rippling.LoadRules();

            //string path1 = Console.ReadLine();
            //string path2 = Console.ReadLine();

            ////create wave rules
            ////WaveRule.CreateWaveRules(path1, path2, "DistribRRev");
            ////Console.ReadLine();
            ////return;

            //XmlDocument x1 = new XmlDocument();
            //XmlDocument x2 = new XmlDocument();

            //x1.Load(path1);
            //x2.Load(path2);

            //Expression e1 = new Expression(x1.DocumentElement, false);
            //Expression e2 = new Expression(x2.DocumentElement, false);
            //Console.WriteLine("Result: " + Rippling.Eq(e1, e2));

            //Console.ReadLine();
        }
    }
}
