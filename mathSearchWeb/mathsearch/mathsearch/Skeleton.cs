﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mathsearch
{
    [Serializable()]
    public class Skeleton
    {
        List<BinaryTree> skeleton;

        public Skeleton(BinaryTree tree)
        {
            skeleton = new List<BinaryTree>();
            buildSkeleton(tree, skeleton, null);
        }

        private void buildSkeleton(BinaryTree node, List<BinaryTree> skel, BinaryTree parent)
        {
            if (node == null)
                return;
            BinaryTree newParent = parent;
            if (node.Data.direction == MarkupType.Skeleton)
            {
                if (parent == null)
                {
                    newParent = new BinaryTree(node.Data);
                    skel.Add(newParent);
                }
                else
                {
                    newParent = parent.GuaranteedAdd(node.Data);
                }
            }
            buildSkeleton(node.Left, skel, newParent);
            buildSkeleton(node.Right, skel, newParent);
        }

        public override bool Equals(object obj)
        {
            Skeleton sk = obj as Skeleton;
            if (sk == null)
                return false;
            List<BinaryTree> skeletonSet1 = skeleton;
            List<BinaryTree> skeletonSet2 = sk.skeleton;
            bool[] isTaken = new bool[skeletonSet2.Count];
            for (int i = 0; i < skeletonSet1.Count; i++)
            {
                bool isMatching = false;
                for (int j = 0; j < skeletonSet2.Count; j++)
                    if (!isTaken[j] && skeletonSet1[i] == skeletonSet2[j])
                    {
                        isTaken[j] = true;
                        isMatching = true;
                        break;
                    }
                if (!isMatching)
                    return false;
            }
            //for (int i = 0; i < skeletonSet2.Count; i++)
            //    if (!isTaken[i])
            //        return false;
            return true;
        }

        public static bool operator ==(Skeleton sk1, Skeleton sk2)
        {
            if ((object)sk1 == null && (object)sk2 == null)
                return true;
            else
                if ((object)sk1 == null || (object)sk2 == null)
                    return false;
            return sk1.Equals(sk2);
        }

        public static bool operator !=(Skeleton sk1, Skeleton sk2)
        {
            return !(sk1 == sk2);
        }

        public override string ToString()
        {
            StringBuilder SB = new StringBuilder();
            foreach (BinaryTree t in skeleton)
            {
                SB.AppendLine(t.PlainText());
                SB.AppendLine("-----------");
            }
            return SB.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static int GetDepthOfNodeInSkeleton(BinaryTree node)
        {
            if (node == null)
                throw new ArgumentNullException("node", "Argument is null");
            if (node.Data.direction != MarkupType.Skeleton)
                throw new InvalidOperationException("Node is not in skeleton");
            int res = 1;
            while (node.Parent != null)
            {
                node = node.Parent;
                if (node.Data.direction == MarkupType.Skeleton)
                    res++;
            }
            return res;
        }
    }
}
