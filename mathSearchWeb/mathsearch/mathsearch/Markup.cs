﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace mathsearch
{
    public enum MarkupType
    {
        Undefined,
        WaveFront,
        Skeleton
    }

    public static class Markup
    {
        private static int varCount = 0;

        public static void SetMarkup(BinaryTree target, MarkupType dir)
        {
            target.Data.direction = dir;
            if (target.Left != null)
                SetMarkup(target.Left, dir);
            if (target.Right != null)
                SetMarkup(target.Right, dir);
        }

        public static void SetMarkup(BinaryTree target, MarkupType desiredMarkup, MarkupType changeOnlyThisMarkup)
        {
            if (target.Data.direction == changeOnlyThisMarkup)
                target.Data.direction = desiredMarkup;
            if (target.Left != null)
                SetMarkup(target.Left, desiredMarkup, changeOnlyThisMarkup);
            if (target.Right != null)
                SetMarkup(target.Right, desiredMarkup, changeOnlyThisMarkup);
        }

        public static List<UnificationEntry> Annotate(Expression e1, Expression e2, bool startConveyor, MinimalityOption minOption)
        {
            //cleaning up from all the previous annotations
            SetMarkup(e1.Root, MarkupType.Undefined, MarkupType.Skeleton);
            SetMarkup(e1.Root, MarkupType.Undefined, MarkupType.WaveFront);
            SetMarkup(e2.Root, MarkupType.Undefined, MarkupType.Skeleton);
            SetMarkup(e2.Root, MarkupType.Undefined, MarkupType.WaveFront);
            UnificationEntry initialEntry = new UnificationEntry(e1.Root.DeepCopy(), e2.Root.DeepCopy());
            DUNIFYMiscellanious misc = new DUNIFYMiscellanious(startConveyor, minOption, false);
            DUNIFYPROCESS(initialEntry, ref misc, null);
            if (minOption != MinimalityOption.NoRestrictions)//cleaningup
            {
                int minimalCost = int.MaxValue;
                int minimalSubstCount = int.MaxValue;
                //clean it up or leave not really strongly minimal unifs?
                foreach (UnificationEntry e in misc.Results)
                {
                    if (e.WRCost < minimalCost)
                        minimalCost = e.WRCost;
                    if (e.SubstCount < minimalSubstCount)
                        minimalSubstCount = e.SubstCount;
                }
                for (int i = 0; i < misc.Results.Count; i++)
                {
                    if (minOption != MinimalityOption.SubstMinimal && misc.Results[i].WRCost > minimalCost)
                    {
                        misc.Results.RemoveAt(i);
                        i--;
                    }
                    else
                        if (minOption != MinimalityOption.CostMinimal && misc.Results[i].SubstCount > minimalSubstCount)
                        {
                            misc.Results.RemoveAt(i);
                            i--;
                        }
                }
            }
            return misc.Results;
        }

        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        private static void logDUNIFYFail(string descr, UnificationEntry entry)
        {
            Rippling.log.Add(descr);
            Rippling.log.Add("givenRoot: ");
            Rippling.log.Add(entry.GivenRoot.ToString());
            Rippling.log.Add("given: ");
            Rippling.log.Add(entry.GivenNode.ToString());
            Rippling.log.Add("goalRoot: ");
            Rippling.log.Add(entry.GoalRoot.ToString());
            Rippling.log.Add("goal: ");
            Rippling.log.Add(entry.GoalNode.ToString());
        }

        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        private static void DUNIFYPATHS(UnificationEntry entry, UnificationEntry backupEntry, ref DUNIFYMiscellanious misc, List<bool[][]> pathsToUnify)
        {
            List<bool[][]> newPaths = new List<bool[][]>(pathsToUnify);
            foreach (bool[][] path in pathsToUnify)
            {
                newPaths.Remove(path);
                try
                {
                    DUNIFYPROCESS(entry.MoveByPath(path), ref misc, new List<bool[][]>(newPaths));
                }
                catch (NotImplementedException)
                {
                    logDUNIFYFail("DUNIFYPATHS messed up " + true, backupEntry);
                    throw;
                }
            }
        }

        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        private static void DUNIFYSIBLINGS(UnificationEntry newEntry, UnificationEntry backupEntry, ref DUNIFYMiscellanious misc, List<bool[][]> pathsToUnify, bool makeCopy, string Descr)
        {
            List<bool[][]> pathsIncreased = new List<bool[][]>(pathsToUnify != null ? pathsToUnify : new List<bool[][]>(1));
            bool[][] newpath = new bool[2][];
            newpath[0] = newEntry.GivenNode.Right == null ? null : newEntry.GivenNode.Right.PathFromRoot;
            newpath[1] = newEntry.GoalNode.Right == null ? null : newEntry.GoalNode.Right.PathFromRoot;
            pathsIncreased.Add(newpath);
            BinaryTree newGiven = newEntry.GivenNode;
            BinaryTree newGoal = newEntry.GoalNode;
            try
            {
                newEntry.GoalNode = newGoal.Left;
                newEntry.GivenNode = newGiven.Left;
                DUNIFYPROCESS(newEntry, ref misc, pathsIncreased);
            }
            catch (NotImplementedException)
            {
                logDUNIFYFail(Descr + " ON LEFT messed up " + makeCopy, backupEntry);
                throw;
            }

            try
            {
                newEntry.GoalNode = newGoal.Right;
                newEntry.GivenNode = newGiven.Right;
                DUNIFYPROCESS(newEntry, ref misc, pathsToUnify);
            }
            catch (NotImplementedException)
            {
                logDUNIFYFail(Descr + " ON Right messed up " + makeCopy, backupEntry);
                throw;
            }
        }

        private static void DUNIFYPROCESS(UnificationEntry entry, ref DUNIFYMiscellanious misc, List<bool[][]> pathsToUnify)
        {
            if (entry.GivenNode != null && entry.GivenNode.Data.direction != MarkupType.Undefined || entry.GoalNode != null && entry.GoalNode.Data.direction != MarkupType.Undefined)
            {
                logDUNIFYFail("markup messed up", entry);
                throw new NotImplementedException("Something went wrong");
            }

            if (misc.IsUnificationFinished)
                return;
            if (misc.MinimalUnificationComplete && ((misc.SubstMinimal && entry.SubstCount > misc.MinimalUnificationSubstCount) || (misc.CostMinimal && entry.WRCost > misc.MinimalUnificationCost)))
            {
                misc.Results.Remove(entry);
                return;
            }

            if (pathsToUnify != null && pathsToUnify.Count == 0)
                pathsToUnify = null;

            if (entry.GivenNode == null && entry.GoalNode != null)
            {
                SetMarkup(entry.GoalNode, MarkupType.WaveFront);
                return;
            }
            else
                if (entry.GivenNode != null && entry.GoalNode == null)
                {
                    SetMarkup(entry.GivenNode, MarkupType.WaveFront);
                    return;
                }
                else
                    if (entry.GivenNode == null && entry.GoalNode == null)
                        return;


            bool makeCopy = false;
            List<UnificationEntry> newEntries = new List<UnificationEntry>();
            UnificationEntry backupEntry = new UnificationEntry(entry);

            //DECOMPOSE/DELETE
            if (backupEntry.GivenNode.Data == backupEntry.GoalNode.Data)
            {
                UnificationEntry newEntry = entry;
                newEntry.IncreaseSimilarCount();

                DUNIFYSIBLINGS(newEntry, backupEntry, ref misc, pathsToUnify, makeCopy, "DECOMPOSE/DELETE");

                makeCopy = true;
                if (misc.ForcedDECOMPOSEDELETE)
                    return;
            }

            #region ELIMINATE
            //ELIMINATEL
            if (backupEntry.GivenNode.Data.type == ElementType.Variable && backupEntry.GoalNode.Find(backupEntry.GivenNode.Data) == null)
            {
                UnificationEntry newEntry;
                if (makeCopy)
                {
                    newEntry = new UnificationEntry(backupEntry);
                    misc.Results.Add(newEntry);
                    newEntries.Add(newEntry);
                }
                else
                    newEntry = entry;

                newEntry.GivenRoot.ReplaceSpecificNodes(backupEntry.GivenNode.Data, newEntry.GoalNode, true);
                newEntry.GoalRoot.ReplaceSpecificNodes(backupEntry.GivenNode.Data, newEntry.GoalNode, true);
                newEntry.IncreaseSubstCount();

                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }

            //ELIMINATER
            if (backupEntry.GoalNode.Data.type == ElementType.Variable && backupEntry.GivenNode.Find(backupEntry.GoalNode.Data) == null)
            {
                UnificationEntry newEntry;
                if (makeCopy)
                {
                    newEntry = new UnificationEntry(backupEntry);
                    misc.Results.Add(newEntry);
                    newEntries.Add(newEntry);
                }
                else
                    newEntry = entry;

                newEntry.GivenRoot.ReplaceSpecificNodes(backupEntry.GoalNode.Data, newEntry.GivenNode, true);
                newEntry.GoalRoot.ReplaceSpecificNodes(backupEntry.GoalNode.Data, newEntry.GivenNode, true);
                newEntry.IncreaseSubstCount();

                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }
            #endregion
            #region IMITATE
            //IMITATEL
            if (backupEntry.GivenNode.Data.type == ElementType.Variable && backupEntry.GoalNode.Data.type == ElementType.Function && backupEntry.GoalNode.Find(backupEntry.GivenNode.Data) == null)
            {
                UnificationEntry newEntry;
                if (makeCopy)
                {
                    newEntry = new UnificationEntry(backupEntry);
                    misc.Results.Add(newEntry);
                    newEntries.Add(newEntry);
                }
                else
                    newEntry = entry;

                BinaryTree treeSubst = new BinaryTree(newEntry.GoalNode.Data);
                if (newEntry.GoalNode.Left != null)
                    treeSubst.Add(new Variable("newvar" + (++varCount).ToString()));
                if (newEntry.GoalNode.Right != null)
                    treeSubst.Add(new Variable("newvar" + (++varCount).ToString()));
                newEntry.GivenRoot.ReplaceSpecificNodes(backupEntry.GivenNode.Data, treeSubst, true);
                newEntry.GoalRoot.ReplaceSpecificNodes(backupEntry.GivenNode.Data, treeSubst, true);
                newEntry.IncreaseSubstCount();

                DUNIFYSIBLINGS(newEntry, backupEntry, ref misc, pathsToUnify, makeCopy, "IMITATEL");

                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }

            //IMITATER
            if (backupEntry.GivenNode.Data.type == ElementType.Function && backupEntry.GoalNode.Data.type == ElementType.Variable && backupEntry.GivenNode.Find(backupEntry.GoalNode.Data) == null)
            {
                UnificationEntry newEntry;
                if (makeCopy)
                {
                    newEntry = new UnificationEntry(backupEntry);
                    misc.Results.Add(newEntry);
                    newEntries.Add(newEntry);
                }
                else
                    newEntry = entry;

                BinaryTree targetSubst = new BinaryTree(newEntry.GivenNode.Data);
                if (newEntry.GivenNode.Left != null)
                    targetSubst.Add(new Variable("newvar" + (++varCount).ToString()));
                if (newEntry.GivenNode.Right != null)
                    targetSubst.Add(new Variable("newvar" + (++varCount).ToString()));
                newEntry.GivenRoot.ReplaceSpecificNodes(backupEntry.GoalNode.Data, targetSubst, true);
                newEntry.GoalRoot.ReplaceSpecificNodes(backupEntry.GoalNode.Data, targetSubst, true);
                newEntry.IncreaseSubstCount();

                DUNIFYSIBLINGS(newEntry, backupEntry, ref misc, pathsToUnify, makeCopy, "IMITATER");

                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }
            #endregion
            #region HIDE
            //HIDELFORL
            if (backupEntry.GoalNode.Data.type == ElementType.Function && backupEntry.GoalNode.Left != null)// && backupEntry.GivenNode.Data.type!=ElementType.Variable)
            {
                UnificationEntry newEntry;
                if (makeCopy)
                {
                    newEntry = new UnificationEntry(backupEntry);
                    misc.Results.Add(newEntry);
                    newEntries.Add(newEntry);
                }
                else
                    newEntry = entry;

                newEntry.GoalNode.Data.direction = MarkupType.WaveFront;
                SetMarkup(newEntry.GoalNode.Left, MarkupType.WaveFront);
                newEntry.IncreaseCost();

                try
                {
                    newEntry.GoalNode = newEntry.GoalNode.Right;
                    DUNIFYPROCESS(newEntry, ref misc, pathsToUnify);
                }
                catch (NotImplementedException)
                {
                    logDUNIFYFail("HIDELL DUNIFY(newTree, newTarget.Right) messed up" + makeCopy, backupEntry);
                    throw;
                }

                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }

            //HIDELFORR
            if (backupEntry.GoalNode.Data.type == ElementType.Function && backupEntry.GoalNode.Right != null)// && backupEntry.GivenNode.Data.type != ElementType.Variable)
            {
                UnificationEntry newEntry;
                if (makeCopy)
                {
                    newEntry = new UnificationEntry(backupEntry);
                    misc.Results.Add(newEntry);
                    newEntries.Add(newEntry);
                }
                else
                    newEntry = entry;

                newEntry.GoalNode.Data.direction = MarkupType.WaveFront;
                SetMarkup(newEntry.GoalNode.Right, MarkupType.WaveFront);
                newEntry.IncreaseCost();

                try
                {
                    newEntry.GoalNode = newEntry.GoalNode.Left;
                    DUNIFYPROCESS(newEntry, ref misc, pathsToUnify);
                }
                catch (NotImplementedException)
                {
                    logDUNIFYFail("HIDELR DUNIFY(newTree, newTarget.Left) messed up" + makeCopy, backupEntry);
                    throw;
                }

                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }

            //HIDERFORL
            if (backupEntry.GivenNode.Data.type == ElementType.Function && backupEntry.GivenNode.Left != null)// && backupEntry.GoalNode.Data.type!=ElementType.Variable)
            {
                UnificationEntry newEntry;
                if (makeCopy)
                {
                    newEntry = new UnificationEntry(backupEntry);
                    misc.Results.Add(newEntry);
                    newEntries.Add(newEntry);
                }
                else
                    newEntry = entry;

                newEntry.GivenNode.Data.direction = MarkupType.WaveFront;
                SetMarkup(newEntry.GivenNode.Left, MarkupType.WaveFront);
                newEntry.IncreaseCost();

                try
                {
                    newEntry.GivenNode = newEntry.GivenNode.Right;
                    DUNIFYPROCESS(newEntry, ref misc, pathsToUnify);
                }
                catch (NotImplementedException)
                {
                    logDUNIFYFail("HIDERL DUNIFY(newTree.Right, newTarget) messed up" + makeCopy, backupEntry);
                    throw;
                }

                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }

            //HIDERFORR
            if (backupEntry.GivenNode.Data.type == ElementType.Function && backupEntry.GivenNode.Right != null)// && backupEntry.GoalNode.Data.type != ElementType.Variable)
            {
                UnificationEntry newEntry;
                if (makeCopy)
                {
                    newEntry = new UnificationEntry(backupEntry);
                    misc.Results.Add(newEntry);
                    newEntries.Add(newEntry);
                }
                else
                    newEntry = entry;

                newEntry.GivenNode.Data.direction = MarkupType.WaveFront;
                SetMarkup(newEntry.GivenNode.Right, MarkupType.WaveFront);
                newEntry.IncreaseCost();

                try
                {
                    newEntry.GivenNode = newEntry.GivenNode.Left;
                    DUNIFYPROCESS(newEntry, ref misc, pathsToUnify);
                }
                catch (NotImplementedException)
                {
                    logDUNIFYFail("HIDERR DUNIFY(newTree.Left, newTarget) messed up" + makeCopy, backupEntry);
                    throw;
                }
                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }
            #endregion

            if (!makeCopy)
            {
                logDUNIFYFail("NO RULES APPLIED", backupEntry);
                Rippling.log.FlushOnScreen();
                throw new ApplicationException("DUNIFY Gone bad " + backupEntry.GivenNode.Data + " " + backupEntry.GoalNode.Data);
            }
            //unification ended
            if (misc.CostMinimal || misc.SubstMinimal || misc.ProceedWithRippling)
            {
                foreach (UnificationEntry e in newEntries)
                    if (e.CompleteUnification())
                    {
                        misc.MinimalUnificationComplete = true;
                        if (misc.CostMinimal && e.WRCost <= misc.MinimalUnificationCost)
                            misc.MinimalUnificationCost = e.WRCost;
                        if (misc.SubstMinimal && e.SubstCount <= misc.MinimalUnificationSubstCount)
                            misc.MinimalUnificationSubstCount = e.SubstCount;
                        if (misc.ProceedWithRippling)
                            e.StartRippling(misc);
                    }
            }
        }
    }
}