﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using mathsearch;

namespace RuleCreator
{
    class Program
    {
        static void Main(string[] args) 
        {
            //BinaryTree t1 = new BinaryTree(new Function("times", WaveFrontDirection.WaveFrontUp));
            //t1.Add(new Function("plus", WaveFrontDirection.WaveFrontUp));
            //t1.Left.Add(new Variable("a", WaveFrontDirection.Base));
            //t1.Left.Add(new Variable("b", WaveFrontDirection.Base));
            //t1.Add(new Function("plus", WaveFrontDirection.WaveFrontUp));
            //t1.Right.Add(new Variable("c", WaveFrontDirection.Base));
            //t1.Right.Add(new Variable("d", WaveFrontDirection.Base));
            //Measure m = null;
            //DateTime before = DateTime.Now;
            //m = new Measure(t1);
            //DateTime after = DateTime.Now;
            //Console.WriteLine("Straight: " + m + " ms: " + (after - before).Milliseconds);
            //Console.WriteLine("Inverted: " + m.Invert());
            ////Console.WriteLine(imprint);
            //Console.ReadLine();
            //return;
            //t1.Add(new Function("times", WaveFrontDirection.Base));
            //t1.Right.Add(new Function("plus", WaveFrontDirection.Base));
            //t1.Right.Left.Add(new Variable("b", WaveFrontDirection.WaveFrontUp));
            //t1.Right.Left.Add(new Variable("c", WaveFrontDirection.WaveFrontUp));
            //t1.Right.Add(new Variable("e", WaveFrontDirection.Base));

            //BinaryTree t2 = new BinaryTree(new Function("plus", WaveFrontDirection.Base));
            //t2.Add(new Variable("a", WaveFrontDirection.WaveFrontUp));
            //t2.Add(new Function("times", WaveFrontDirection.Base));
            //t2.Right.Add(new Function("plus", WaveFrontDirection.Base));
            //t2.Right.Left.Add(new Variable("e", WaveFrontDirection.WaveFrontUp));
            //t2.Right.Left.Add(new Variable("w", WaveFrontDirection.WaveFrontUp));
            //t2.Right.Add(new Variable("e", WaveFrontDirection.Base));
            //Dictionary<Variable, HashSet<Variable>> relTable = new Dictionary<Variable, HashSet<Variable>>();
            //Console.WriteLine("Function result: " + Rippling.buildRelationTable(t1, t2, ref relTable, false));
            //Console.WriteLine();
            //foreach (KeyValuePair<Variable, HashSet<Variable>> pair in relTable)
            //{
            //    Console.Write(pair.Key.name + ": ");
            //    foreach (Variable v in pair.Value)
            //        Console.Write(v.name + " ");
            //    Console.WriteLine();
            //}
            //Console.WriteLine("CheckEQ result: " + Rippling.checkEqAndCorelateVars(t1, t2));
            //Console.ReadLine();
            //return;
             

            ///WAVERULECREATION///
            ///DAWG
           // WaveRule.CreateWaveRules("WR/1sumcommutativity1.txt", "WR/1sumcommutativity2.txt", "sumcommut1");
           // WaveRule.CreateWaveRules("WR/1sumcommutativity2.txt", "WR/1sumcommutativity1.txt", "sumcommut2");
           // WaveRule.CreateWaveRules("WR/2sumassoc1.txt", "WR/2sumassoc2.txt", "sumassoc1");
           // WaveRule.CreateWaveRules("WR/2sumassoc2.txt", "WR/2sumassoc1.txt", "sumassoc2");
           // WaveRule.CreateWaveRules("WR/3zero1.txt", "WR/3zero2.txt", "zero1");
           // WaveRule.CreateWaveRules("WR/3zero2.txt", "WR/3zero1.txt", "zero2");
           //// WaveRule.CreateWaveRules("WR/4suminverse1.txt", "WR/4suminverse2.txt", "suminverse1");
           // //WaveRule.CreateWaveRules("WR/4suminverse2.txt", "WR/4suminverse1.txt", "suminverse2");
           // WaveRule.CreateWaveRules("WR/5mulcommutativity1.txt", "WR/5mulcommutativity2.txt", "mulcommut1");
           // WaveRule.CreateWaveRules("WR/5mulcommutativity2.txt", "WR/5mulcommutativity1.txt", "mulcommut2");
           // WaveRule.CreateWaveRules("WR/6mulassoc1.txt", "WR/6mulassoc2.txt", "mulassoc1");
           // WaveRule.CreateWaveRules("WR/6mulassoc2.txt", "WR/6mulassoc1.txt", "mulassoc2");
           // WaveRule.CreateWaveRules("WR/7one1.txt", "WR/7one2.txt", "one1");
           // WaveRule.CreateWaveRules("WR/7one2.txt", "WR/7one1.txt", "one2");
           // //WaveRule.CreateWaveRules("WR/8mulinverse1.txt", "WR/8mulinverse2.txt", "mulinverse1");
           // //WaveRule.CreateWaveRules("WR/8mulinverse2.txt", "WR/8mulinverse1.txt", "mulinverse2");
           // WaveRule.CreateWaveRules("WR/9distrib1.txt", "WR/9distrib2.txt", "distrib1");
           // WaveRule.CreateWaveRules("WR/9distrib2.txt", "WR/9distrib1.txt", "distrib2");
            //return;
            ///ENDOFWAVERULECREATION
            Console.WriteLine("Started "+Rippling.LoadRules("rules"));
            Rippling.log.FlushOnScreen();
            string path1 = Console.ReadLine();
            string path2 = Console.ReadLine();

            //create wave rules
            //WaveRule.CreateWaveRules(path1, path2, "sumcommut1");
            //Console.ReadLine();
            //return;
    
            Expression e1 = new Expression(File.ReadAllText(path1));
            Expression e2 = new Expression(File.ReadAllText(path2));
            //List<BinaryTree> skeleton = e1.Skeleton;
            //foreach (BinaryTree t in skeleton)
            //    t.PlainPrint();
            //e1.Root.Left.Left.Data.direction = WaveFrontDirection.WaveFrontUp;
            //skeleton = e1.Skeleton;
            //foreach (BinaryTree t in skeleton)
            //    t.PlainPrint();
            //Console.ReadLine();
            //return;
            DateTime start = DateTime.Now;
            Console.WriteLine("Result: "+Rippling.Eq(e1, e2, 0.85,false,false, MeasureComparisonMode.Shrunk));
            //Console.WriteLine("Total: "+Rippling.UnifEntryCount(e1, e2, false));
            DateTime finish = DateTime.Now;
            Console.WriteLine("Duration: " + (finish - start).TotalMilliseconds + " ms");
            Console.ReadLine();
        }
    }
}
